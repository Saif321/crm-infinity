import { TestBed } from '@angular/core/testing';

import { ReportPageService } from './report-page.service';

describe('ReportPageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportPageService = TestBed.inject(ReportPageService);
    expect(service).toBeTruthy();
  });
});
