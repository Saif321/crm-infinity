import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';

import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgSelectModule } from '@ng-select/ng-select';

import { SharedModule } from '@shared/shared.module';
import { InteractionModule } from '@app/interaction/interaction.module';

import { ReportPageRoutingModule } from './report-page-routing.module';
import { ListComponent } from './list/list.component';
import { ReportPageService } from './report-page.service';

@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    ReportPageRoutingModule,
    SharedModule,
    InteractionModule,
    ModalModule.forRoot(),
    FormsModule,
    PaginationModule.forRoot(),
    NgSelectModule
  ],
  providers: [ReportPageService]
})
export class ReportPageModule { }
