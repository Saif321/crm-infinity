import { Injectable } from '@angular/core';

import { FitApiClientService, FitList } from '@shared/fit-api-client.service';

import { LevelBase, LevelService } from '@app/level/level.service';
import { GoalBase, GoalService } from '@app/goal/goal.service';
import { EnvironmentBase, EnvironmentService } from '@app/environment/environment.service';

interface ReportBase {
  first_name: string;
  last_name: string;
  level: string;
  goals: string;
  environment: string;
  age: string;
  gender: string;
}

export { FitList, ReportBase, LevelBase, GoalBase, EnvironmentBase };

@Injectable()
export class ReportPageService {
  constructor(
    private fitApi: FitApiClientService,
    private levelService: LevelService,
    private goalService: GoalService,
    private environmentService: EnvironmentService) { }


  public search(page: number, environment_ids?: number[], level_ids?: number[], goal_ids?: number[], gender?: string, age_from?: number, age_to?: number) {
    var params: any = {
      page: page.toString()
    }
    if (Array.isArray(environment_ids)) {
      params['environmentIds[]'] = environment_ids;
    }
    if (Array.isArray(level_ids)) {
      params['levelIds[]'] = level_ids;
    }
    if (Array.isArray(goal_ids)) {
      params['goalIds[]'] = goal_ids;
    }
    if (gender) {
      params['gender'] = gender;
    }
    if (age_from) {
      params['age_from'] = age_from;
    }
    if (age_to) {
      params['age_to'] = age_to;
    }
    return this.fitApi.get<FitList<ReportBase>>('/admin/reports/customers', params);
  }

  public searchLevels(query: string, page: number) {
    return this.levelService.search(query, page);
  }

  public searchGoals(query: string, page: number) {
    return this.goalService.search(query, page);
  }

  public searchEnvironments(query: string, page: number) {
    return this.environmentService.search(query, page);
  }


}
