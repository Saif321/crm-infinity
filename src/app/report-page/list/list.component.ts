import { Component, EventEmitter } from '@angular/core';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';

import { ReportPageService, ReportBase, LevelBase, GoalBase, EnvironmentBase } from '../report-page.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

interface Gender {
  key: string;
  label: string;
}

const male = { key: 'male', label: 'Male' };
const female = { key: 'female', label: 'Female' };
const totalCustomers = 0;

@Component({
  selector: 'report-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends PageableComponent<ReportBase> {
  constructor(
    protected reportPageService: ReportPageService,
    private interaction: InteractionService
  ) {
    super();
    this.load();
    this.loadLevels();
    this.loadGoals();
    this.initEnvironmentsSearch();
  }

  public environmentsTypeAhead = new EventEmitter<string>();
  public environments: EnvironmentBase[] = [];
  public environmentIds: number[] = [];

  public gendersTypeAhead = new EventEmitter<string>();
  public genderOptions: Gender[] = [male, female];
  public gender: string;

  public goalsTypeAhead = new EventEmitter<string>();
  public goals: GoalBase[] = [];
  public goal_ids: number[] = [];

  public levelsTypeAhead = new EventEmitter<string>();
  public levels: LevelBase[] = [];
  public level_ids: number[] = [];

  public age_from: number;
  public age_to: number;

  public showFilters = false;

  public async load() {
    const { data } = await this.reportPageService.search(
      this._page,
      this.environmentIds,
      this.level_ids,
      this.goal_ids,
      this.gender,
      this.age_from,
      this.age_to
    );
    this._list = data
  }

  private initEnvironmentsSearch() {
    this.environmentsTypeAhead.pipe(
      distinctUntilChanged(),
      debounceTime(400)
    ).subscribe(term => {
      this.reportPageService.searchEnvironments(term, 1)
        .then(({ data, error }) => {
          this.environments = [];
          if (!error) {
            this.environments = data.data;
          }
        });
    });
  }

  private async loadLevels() {
    const { data, error } = await this.reportPageService.searchLevels('', 1);
    if (!error) {
      this.levels = data.data;
    }
  }

  private async loadGoals() {
    const { data, error } = await this.reportPageService.searchGoals('', 1);
    if (!error) {
      this.goals = data.data;
    }
  }

}
