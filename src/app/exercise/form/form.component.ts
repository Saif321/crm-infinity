import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { distinctUntilChanged, debounceTime } from 'rxjs/operators';

import { LanguagesComponent } from '@shared/components/languages/languages.component';
import { NotificationsService } from '@shared/notifications.service';

import { ExerciseService, Exercise, ExerciseTranslation, MuscleGroupBase, EnvironmentBase, LevelBase, ExerciseRequest } from '../exercise.service';
import { ExerciseStatus, statuses } from '../exercise-status';

@Component({
    selector: 'exercise-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
    constructor(
        private exercises: ExerciseService,
        private route: ActivatedRoute,
        private router: Router,
        private notifications: NotificationsService
    ) {
        this.initMuscleGroupSearch();
        this.initEnvironmentsSearch();
        this.loadLevels();
    }

    public selectedLanguageId = 1;
    public isDisabled = false;

    public muscleGroupsTypeAhead = new EventEmitter<string>();
    public muscleGroups: MuscleGroupBase[] = [];

    public environmentsTypeAhead = new EventEmitter<string>();
    public environments: EnvironmentBase[] = [];

    public statuses: ExerciseStatus[] = statuses;
    public levels: LevelBase[] = [];

    @ViewChild(LanguagesComponent, { static: true })
    languagesComponent: LanguagesComponent;

    public isEditMode = false;

    public get isDefaultLanguage() {
        return this.languagesComponent.isDefaultLanguage;
    }

    public get exercise() {
        return this._exercise;
    }

    public get exerciseTranslation(): ExerciseTranslation {
        const exercise = this._exercise;
        if (!exercise) {
            return;
        }
        let translation = exercise.translations.find(
            t => t.language_id === this.selectedLanguageId
        );
        if (!translation) {
            translation = {
                description: exercise.description,
                exercise_environment_ids: exercise.exercise_environment_ids,
                image: undefined,
                image_id: undefined,
                is_free: exercise.is_free,
                is_published: exercise.is_published,
                name: exercise.name,
                level_ids: [],
                muscle_group_ids: exercise.muscle_group_ids,
                language_id: this.selectedLanguageId,
                video: undefined,
                video_id: undefined
            };
            exercise.translations.push(translation);
        }
        return translation;
    }

    public onExerciseSubmit() {
        const defaultTranslation = this.getDefaultTranslation();
        this._exercise.name = defaultTranslation.name;
        this._exercise.description = defaultTranslation.description;
        this._exercise.image_id = defaultTranslation.image.id;
        this._exercise.video_id = defaultTranslation.video ? defaultTranslation.video.id : null;
        if (this.isEditMode) {
            this.update();
        } else {
            this.create();
        }
    }

    public async ngOnInit() {
        this.route.paramMap.subscribe(params => {
            if (!params.has('id')) {
                this.isEditMode = false;
                return;
            }
            this.isEditMode = true;
            this.exercises.get(parseInt(params.get('id'))).then(res => {
                this._exercise = res.data;
                this.muscleGroups = res.data.muscle_groups;
                this._exercise.muscle_group_ids = res.data.muscle_groups.map(m => m.id);
                this.environments = res.data.exercise_environments;
                this._exercise.exercise_environment_ids = res.data.exercise_environments.map(e => e.id);
                this._exercise.level_ids = res.data.levels.map(l => l.id);
            });
        });
    }

    public async create() {
        this.isDisabled = true;
        const { data, error } = await this.exercises.create(this.mapToExerciseRequest());
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'Create was successful!');
            this.router.navigate(['/exercise/edit', data.id]);
        }
    }

    public async update() {
        this.isDisabled = true;
        const { data, error } = await this.exercises.update(this.mapToExerciseRequest());
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'The update was successful!');
        }
    }

    private initMuscleGroupSearch() {
        this.muscleGroupsTypeAhead.pipe(
            distinctUntilChanged(),
            debounceTime(400)
        ).subscribe(term => {
            this.exercises.searchMuscleGroups(term, 1)
                .then(({ data, error }) => {
                    this.muscleGroups = [];
                    if (!error) {
                        this.muscleGroups = data.data;
                    }
                });
        });
    }

    private initEnvironmentsSearch() {
        this.environmentsTypeAhead.pipe(
            distinctUntilChanged(),
            debounceTime(400)
        ).subscribe(term => {
            this.exercises.searchEnvironments(term, 1)
                .then(({ data, error }) => {
                    this.muscleGroups = [];
                    if (!error) {
                        this.environments = data.data;
                    }
                });
        });
    }

    private async loadLevels() {
        const { data, error } = await this.exercises.searchLevels('', 1);
        if (!error) {
            this.levels = data.data;
        }
    }

    private getDefaultTranslation() {
        return this._exercise.translations.find(t => t.language_id === this.languagesComponent.defaultLanguage.id);
    }

    private mapToExerciseRequest(): ExerciseRequest {
        const defaultTranslation = this.getDefaultTranslation();
        return {
            id: this.exercise.id,
            name: defaultTranslation.name,
            description: defaultTranslation.description,
            image_id: defaultTranslation.image.id,
            video_id: defaultTranslation.video ? defaultTranslation.video.id : null,
            exercise_environment_ids: this._exercise.exercise_environment_ids,
            muscle_group_ids: this._exercise.muscle_group_ids,
            is_free: this._exercise.is_free,
            is_published: this._exercise.is_published,
            level_ids: this._exercise.level_ids,
            translations: this._exercise.translations.filter(t => t.name && t.description)
                .map(t => {
                    return {
                        language_id: t.language_id,
                        name: t.name,
                        description: t.description,
                        video_id: t.video ? t.video.id : null,
                        image_id: t.image.id,
                    }
                })
        };
    }

    private _exercise: Exercise = {
        description: '',
        image_id: null,
        is_free: false,
        is_published: false,
        name: '',
        level_ids: [],
        video_id: null,
        translations: [],
        exercise_environment_ids: [],
        muscle_group_ids: []
    };
}
