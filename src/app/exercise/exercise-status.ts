interface ExerciseStatus {
    name: string;
    value: boolean;
}

const statuses: ExerciseStatus[] = [
    { value: false, name: 'Draft' },
    { value: true, name: 'Published' }
];

export { ExerciseStatus, statuses };