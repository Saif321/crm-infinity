import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';

import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgSelectModule } from '@ng-select/ng-select';

import { SharedModule } from '@shared/shared.module';
import { InteractionModule } from '@app/interaction/interaction.module';
import { GalleryModule } from '@app/gallery/gallery.module';

import { ExerciseRoutingModule } from './exercise-routing.module';
import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';
import { ExerciseService } from './exercise.service';
import { ModalComponent } from './modal/modal/modal.component';
import { ModalBodyComponent } from './modal/modal-body/modal-body.component';

@NgModule({
    declarations: [ListComponent, FormComponent, ModalComponent, ModalBodyComponent],
    imports: [
        CommonModule,
        ExerciseRoutingModule,
        ModalModule.forRoot(),
        FormsModule,
        SharedModule,
        InteractionModule,
        PaginationModule.forRoot(),
        GalleryModule,
        NgSelectModule
    ],
    providers: [ExerciseService]
})
export class ExerciseModule { }
