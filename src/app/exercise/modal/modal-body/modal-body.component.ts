import { Component, Output, EventEmitter, Input } from '@angular/core';
import { PageableComponent } from '@shared/pageable.component';

import { ExerciseService, ExerciseBase, MuscleGroupBase } from '../../exercise.service';

import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'modal-body',
  templateUrl: './modal-body.component.html',
  styleUrls: ['./modal-body.component.scss']
})
export class ModalBodyComponent extends PageableComponent<ExerciseBase> {

  constructor(
    protected exerciseService: ExerciseService
  ) {
    super();
    this.load();
    this.initMuscleGroupSearch();
  }

  public muscleGroupsTypeAhead = new EventEmitter<string>();
  public muscleGroups: MuscleGroupBase[] = [];
  public muscleGroupIds: number[] = [];

  @Output() public itemSelected = new EventEmitter<ExerciseBase>(true);

  @Input() public selected: ExerciseBase;

  public async load() {
    const { data } = await this.exerciseService.search(
      this.searchQuery,
      this._page,
      this.muscleGroupIds,
      undefined,
      true
    );
    this._list = data;
  }

  public select(item: ExerciseBase) {
    this.selected = item;
    this.itemSelected.emit(item);
  }

  private initMuscleGroupSearch() {
    this.muscleGroupsTypeAhead.pipe(
      distinctUntilChanged(),
      debounceTime(400)
    ).subscribe(term => {
      this.exerciseService.searchMuscleGroups(term, 1)
        .then(({ data, error }) => {
          this.muscleGroups = [];
          if (!error) {
            this.muscleGroups = data.data;
          }
        });
    });
  }
}
