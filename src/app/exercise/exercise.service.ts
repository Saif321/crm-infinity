import { Injectable } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { FitApiClientService, FitList } from '@shared/fit-api-client.service';
import { GalleryItem } from '@app/gallery/gallery.service';

import { MuscleGroupService, MuscleGroupBase } from '@app/muscle-group/muscle-group.service';
import { EnvironmentBase, EnvironmentService } from '@app/environment/environment.service';
import { LevelBase, LevelService } from '@app/level/level.service';
import { ModalComponent } from './modal/modal/modal.component';

interface ExerciseBase {
    created_at?: string;
    description: string;
    exercise_environments?: EnvironmentBase[];
    exercise_environment_ids: number[];
    id?: number;
    image?: GalleryItem;
    image_id: number;
    is_free: boolean;
    name: string;
    is_published: boolean;
    level_ids: number[];
    levels?: LevelBase[];
    muscle_groups?: MuscleGroupBase[];
    muscle_group_ids: number[];
    updated_at?: string;
    video?: GalleryItem;
    video_id: number;

}

interface ExerciseTranslation extends ExerciseBase {
    language_id: number;
    object_id?: number;
}

interface Exercise extends ExerciseBase {
    translations: ExerciseTranslation[];
}

export interface ExerciseTranslationRequest {
    language_id: number;
    name: string;
    description: string;
    video_id: number;
    image_id: number;
}

export interface ExerciseRequest {
    id?: number;
    name: string;
    description: string;
    video_id: number;
    image_id: number;
    is_published: boolean;
    is_free: boolean;
    level_ids: number[];
    exercise_environment_ids: number[];
    muscle_group_ids: number[];
    translations: ExerciseTranslationRequest[]
}

export { FitList, ExerciseBase, Exercise, ExerciseTranslation, MuscleGroupBase, EnvironmentBase, LevelBase };

@Injectable()
export class ExerciseService {
    constructor(
        private fitApi: FitApiClientService,
        private muscleGroupService: MuscleGroupService,
        private environmentService: EnvironmentService,
        private levelService: LevelService,
        private modalService: BsModalService) { }

    private bsModalRef: BsModalRef;
    public search(query: string, page: number, muscle_group_ids?: number[], environment_ids?: number[], is_published?: boolean, is_free?: boolean) {
        var params: any = {
            search: query,
            page: page.toString()
        }
        if (is_published === true || is_published === false) {
            params.is_published = is_published === true ? 'true' : 'false'
        }
        if (is_free === true || is_free === false) {
            params.is_free = is_free === true ? 'true' : 'false'
        }
        if (Array.isArray(muscle_group_ids) && muscle_group_ids.length > 0) {
            params.muscle_group_ids = muscle_group_ids.join(',');
        }
        if (Array.isArray(environment_ids) && environment_ids.length > 0) {
            params.environment_ids = environment_ids.join(',');
        }
        return this.fitApi.get<FitList<ExerciseBase>>('/admin/exercise', params);
    }
    
    public get(id: number) {
        return this.fitApi.get<Exercise>('/admin/exercise/' + id);
    }

    public create(exercise: ExerciseRequest) {
        return this.fitApi.put<ExerciseRequest>('/admin/exercise/0', exercise);
    }

    public update(exercise: ExerciseRequest) {
        return this.fitApi.put<ExerciseRequest>('/admin/exercise/' + exercise.id, exercise);
    }

    public delete(id: number) {
        return this.fitApi.delete<any>('/admin/exercise/' + id);
    }

    public publish(exerciseId: number) {
        return this.fitApi.put<any>(`/admin/exercise/${exerciseId}/publish`, null);
    }

    public searchEnvironments(query: string, page: number) {
        return this.environmentService.search(query, page);
    }

    public searchMuscleGroups(query: string, page: number) {
        return this.muscleGroupService.search(query, page);
    }

    public searchLevels(query: string, page: number) {
        return this.levelService.search(query, page);
    }

    public selectFromExercise(): Promise<Exercise> {
        const promise = new Promise<Exercise>((resolve, reject) => {
            const initialState = {
                onClose: (item: Exercise) => {
                    resolve(item);
                },
            };
            this.bsModalRef = this.modalService.show(ModalComponent, { initialState, backdrop: false, ignoreBackdropClick: true, class: 'exercise-modal' });
        })
        return promise;
    }

}
