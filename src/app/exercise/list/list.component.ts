import { Component, EventEmitter } from '@angular/core';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';

import { ExerciseService, ExerciseBase, MuscleGroupBase, EnvironmentBase, LevelBase } from '../exercise.service';
import { ExerciseStatus, statuses } from '../exercise-status';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

interface FreePaidFlag extends ExerciseStatus {

}

@Component({
    selector: 'exercise-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent extends PageableComponent<ExerciseBase> {
    constructor(
        protected exerciseService: ExerciseService,
        private interaction: InteractionService
    ) {
        super();
        this.load();
        this.initMuscleGroupSearch();
        this.initEnvironmentsSearch();
        this.loadLevels();
    }

    public muscleGroupsTypeAhead = new EventEmitter<string>();
    public muscleGroups: MuscleGroupBase[] = [];
    public muscleGroupIds: number[] = [];

    public environmentsTypeAhead = new EventEmitter<string>();
    public environments: EnvironmentBase[] = [];
    public environmentIds: number[] = [];

    public statuses: ExerciseStatus[] = statuses;
    public freePaid: FreePaidFlag[] = [{ value: true, name: 'Free' }, { value: false, name: 'Paid' }];

    public levels: LevelBase[] = [];

    public isPublished: boolean;
    public isFree: boolean;

    public showFilters = false;

    public async publish(exercise: ExerciseBase) {
        let isConfirmed = await this.interaction.confirm(exercise.is_published ? 'Are you sure you want to unpublish exercise?' : 'Are you sure you want to publish exercise?');
        if (isConfirmed) {
            await this.exerciseService.publish(exercise.id);
            this.load();
        }
    }

    public async load() {
        const { data } = await this.exerciseService.search(
            this.searchQuery,
            this._page,
            this.muscleGroupIds,
            this.environmentIds,
            this.isPublished,
            this.isFree
        );
        this._list = data;
    }

    public async delete(id: number) {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to delete this exercise?'
        );
        if (isConfirmed) {
            await this.exerciseService.delete(id);
            this.load();
        }
    }

    public mapLevels(exercise: ExerciseBase){
        return exercise.levels ? exercise.levels.map(l => l.name).join(' / ') : '';
    }

    private initMuscleGroupSearch() {
        this.muscleGroupsTypeAhead.pipe(
            distinctUntilChanged(),
            debounceTime(400)
        ).subscribe(term => {
            this.exerciseService.searchMuscleGroups(term, 1)
                .then(({ data, error }) => {
                    this.muscleGroups = [];
                    if (!error) {
                        this.muscleGroups = data.data;
                    }
                });
        });
    }

    private initEnvironmentsSearch() {
        this.environmentsTypeAhead.pipe(
            distinctUntilChanged(),
            debounceTime(400)
        ).subscribe(term => {
            this.exerciseService.searchEnvironments(term, 1)
                .then(({ data, error }) => {
                    this.environments = [];
                    if (!error) {
                        this.environments = data.data;
                    }
                });
        });
    }

    private async loadLevels() {
        const { data, error } = await this.exerciseService.searchLevels('', 1);
        if (!error) {
            this.levels = data.data;
        }
    }
}
