import { TestBed } from '@angular/core/testing';

import { MealGroupService } from './meal-group.service';

describe('MealGroupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MealGroupService = TestBed.inject(MealGroupService);
    expect(service).toBeTruthy();
  });
});
