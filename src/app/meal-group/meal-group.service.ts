import { Injectable } from '@angular/core';

import { FitApiClientService, FitList } from '@shared/fit-api-client.service';

interface MealGroupBase {
    id?: number;
    name: string;
    eating_time: string;
    updated_at?: string;
    created_at?: string;
}
interface MealGroupTranslation {
    language_id: number;
    name: string;
    eating_time: string;
}
interface MealGroup extends MealGroupBase {
    translations: MealGroupTranslation[];
}

export { FitList, MealGroupBase, MealGroup, MealGroupTranslation };

@Injectable({ providedIn: 'root' })
export class MealGroupService {
    constructor(private fitApi: FitApiClientService) { }

    public search(query: string, page: number) {
        return this.fitApi.get<FitList<MealGroup>>('/admin/meal-group', {
            search: query,
            page: page.toString()
        });
    }

    public get(id: number) {
        return this.fitApi.get<MealGroup>('/admin/meal-group/' + id);
    }

    public create(mealGroup: MealGroup) {
        return this.fitApi.put<MealGroup>('/admin/meal-group/0', mealGroup);
    }

    public update(mealGroup: MealGroup) {
        return this, this.fitApi.put<MealGroup>('/admin/meal-group/' + mealGroup.id, mealGroup);
    }

    public delete(id: number) {
        return this.fitApi.delete<any>('/admin/meal-group/' + id);
    }
}