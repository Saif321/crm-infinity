import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '@shared/shared.module';
import { InteractionModule } from '@app/interaction/interaction.module';

import { MealGroupRoutingModule } from './meal-group-routing.module';

import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';

@NgModule({
    declarations: [ListComponent, FormComponent],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        MealGroupRoutingModule,
        InteractionModule,
        PaginationModule.forRoot()
    ],
    providers: []
})
export class MealGroupModule { }
