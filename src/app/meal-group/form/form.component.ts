import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';

import { LanguagesComponent } from '@shared/components/languages/languages.component';
import { NotificationsService } from '@shared/notifications.service';

import { MealGroupService, MealGroupTranslation, MealGroup } from '../meal-group.service';

@Component({
    selector: 'muscle-group-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
    constructor(
        private mealGroups: MealGroupService,
        private route: ActivatedRoute,
        private router: Router,
        private notifications: NotificationsService
    ) { }

    public selectedLanguageId = 1;

    @ViewChild(LanguagesComponent, { static: true })
    languagesComponent: LanguagesComponent;

    public isEditMode = false;
    public isDisabled = false;

    public get isDefaultLanguage() {
        return this.languagesComponent.isDefaultLanguage;
    }

    public get mealGroup() {
        return this._mealGroup;
    }

    public get mealGroupTranslation(): MealGroupTranslation {
        const mealGroup = this._mealGroup;
        if (!mealGroup) {
            return;
        }
        let translation = mealGroup.translations.find(
            t => t.language_id === this.selectedLanguageId
        );
        if (!translation) {
            translation = {
                name: mealGroup.name,
                language_id: this.selectedLanguageId,
                eating_time: mealGroup.eating_time
            };
            mealGroup.translations.push(translation);
        }
        return translation;
    }

    public onMealGroupSubmit() {
        if (this.isEditMode) {
            this.update();
        } else {
            this.create();
        }
    }

    public async ngOnInit() {
        this.route.paramMap.subscribe(params => {
            if (!params.has('id')) {
                this.isEditMode = false;
                return;
            }
            this.isEditMode = true;
            this.mealGroups.get(parseInt(params.get('id'))).then(res => {
                if (!res.error) {
                    this._mealGroup = res.data;
                }
            });
        });
    }

    public async create() {
        this.isDisabled = true;
        const { data, error } = await this.mealGroups.create(this.getRequest());
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'Create was successful!');
            this.router.navigate(['/meal-group/edit', data.id]);
        }
    }

    public async update() {
        this.isDisabled = true;
        const { data, error } = await this.mealGroups.update(this.getRequest());
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'The update was successful!');
        }
    }

    private getDefaultTranslation() {
        return this._mealGroup.translations.find(t => t.language_id == this.languagesComponent.defaultLanguage.id);
    }

    private getRequest(): MealGroup {
        const defaultTranslation = this.getDefaultTranslation();
        return {
            id: this._mealGroup.id,
            name: defaultTranslation.name,
            eating_time: defaultTranslation.eating_time,
            translations: this._mealGroup.translations.filter(t => t.name && t.eating_time)
                .map(t => {
                    return {
                        language_id: t.language_id,
                        name: t.name,
                        eating_time: t.eating_time
                    }
                })
        }
    }

    private _mealGroup: MealGroup = {
        name: '',
        eating_time: '',
        translations: []
    };
}
