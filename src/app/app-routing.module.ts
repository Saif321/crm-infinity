import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthorizationNeededGuard } from '@shared/guards/authorization-needed.guard';
import { SharedModule } from '@shared/shared.module';

const routes: Routes = [
    { path: '', redirectTo: 'exercise', pathMatch: 'full' },
    { path: 'meal', loadChildren: () => import('./meal/meal.module').then(m => m.MealModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'meal-group', loadChildren: () => import('./meal-group/meal-group.module').then(m => m.MealGroupModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'exercise', loadChildren: () => import('./exercise/exercise.module').then(module => module.ExerciseModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'nutrition-program', loadChildren: () => import('./nutrition-program/nutrition-program.module').then(m => m.NutritionProgramModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'workout', loadChildren: () => import('./workout/workout.module').then(module => module.WorkoutModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'program', loadChildren: () => import('./program/program.module').then(m => m.ProgramModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'goal', loadChildren: () => import('./goal/goal.module').then(module => module.GoalModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'report-page', loadChildren: () => import('./report-page/report-page.module').then(module => module.ReportPageModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'subscription', loadChildren: () => import('./subscription/subscription.module').then(module => module.SubscriptionModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'payment', loadChildren: () => import('./payment/payment.module').then(module => module.PaymentModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'blog', loadChildren: () => import('./blog/blog.module').then(module => module.BlogModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'muscle-group', loadChildren: () => import('./muscle-group/muscle-group.module').then(module => module.MuscleGroupModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'environment', loadChildren: () => import('./environment/environment.module').then(module => module.EnvironmentModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'level', loadChildren: () => import('./level/level.module').then(module => module.LevelModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'review', loadChildren: () => import('./review/review.module').then(module => module.ReviewModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'user', loadChildren: () => import('./user/user.module').then(module => module.UserModule), canActivate: [AuthorizationNeededGuard] },
    { path: 'auth', loadChildren: () => import('./auth/auth.module').then(module => module.AuthModule) },
    { path: '**', redirectTo: '/' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        preloadingStrategy: PreloadAllModules
    }), SharedModule],
    exports: [RouterModule]
})
export class AppRoutingModule { }
