import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { NgProgressRouterModule } from '@ngx-progressbar/router';

import { tokenGetter } from '@shared/authorization.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PipesModule } from './pipes/pipes.module';

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        JwtModule.forRoot({
            config: {
                tokenGetter,
                whitelistedDomains: [
                    'localhost', 
                    'fit-api.telego-dev.net', 
                    'api.infinityfit.io', 
                    'infinity-fit.test:8080'
                ],
                blacklistedRoutes: []
            }
        }),
        PipesModule,
        BsDropdownModule.forRoot(),
        NgProgressModule,
        NgProgressHttpModule,
        NgProgressRouterModule
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
