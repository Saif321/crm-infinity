import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PaginationModule } from 'ngx-bootstrap/pagination';

import { SharedModule } from '@shared/shared.module';
import { InteractionModule } from '@app/interaction/interaction.module';

import { LevelRoutingModule } from './level-routing.module';
import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';

@NgModule({
  declarations: [ListComponent, FormComponent],
  imports: [
    CommonModule,
    LevelRoutingModule,
    CommonModule,
    FormsModule,
    SharedModule,
    InteractionModule,
    PaginationModule.forRoot()
  ],
  providers: []
})
export class LevelModule { }
