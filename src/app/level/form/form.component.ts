import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { LanguagesComponent } from '@shared/components/languages/languages.component';
import { NotificationsService } from '@shared/notifications.service';

import { LevelService, Level, LevelTranslation } from '../level.service';

@Component({
    selector: 'level-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
    constructor(
        private levels: LevelService,
        private route: ActivatedRoute,
        private router: Router,
        private notifications: NotificationsService
    ) { }

    @ViewChild(LanguagesComponent, { static: true }) languagesComponent: LanguagesComponent;

    public selectedLanguageId = 1;
    public isEditMode = false;
    public isDisabled = false;

    public get isDefaultLanguage() {
        return this.languagesComponent.isDefaultLanguage;
    }

    public get level() {
        return this._level;
    }

    public get levelTranslation(): LevelTranslation {
        const level = this._level;
        if (!level) {
            return;
        }
        let translation = level.translations.find(
            t => t.language_id === this.selectedLanguageId
        );
        if (!translation) {
            translation = {
                name: level.name,
                description: level.description,
                language_id: this.selectedLanguageId
            };
            level.translations.push(translation);
        }
        return translation;
    }

    public onLevelSubmit() {
        if (this.isEditMode) {
            this.update();
        } else {
            this.create();
        }
    }

    public async ngOnInit() {
        this.route.paramMap.subscribe(params => {
            if (!params.has('id')) {
                this.isEditMode = false;
                return;
            }
            this.isEditMode = true;
            this.levels.get(parseInt(params.get('id'))).then(res => {
                this._level = res.data;
            });
        });
    }

    public async create() {
        this.isDisabled = true;
        const defaultTranslation = this.getDefaultTranslation();
        this.level.name = defaultTranslation.name;
        this.level.description = defaultTranslation.description;
        const { data, error } = await this.levels.create(this.level);
        this.isDisabled = false
        if (!error) {
            this.notifications.success('Success', 'Create was successful!');
            this.router.navigate(['/level/edit', data.id]);
        }
    }

    public async update() {
        this.isDisabled = true;
        const defaultTranslation = this.getDefaultTranslation();
        this.level.name = defaultTranslation.name;
        this.level.description = defaultTranslation.description;
        const { data, error } = await this.levels.update(this.level);
        this.isDisabled = false
        if (!error) {
            this.notifications.success('Success', 'The update was successful!');
        }
    }

    private getDefaultTranslation() {
        return this._level.translations.find(t => t.language_id == this.languagesComponent.defaultLanguage.id);
    }

    private _level: Level = {
        name: '',
        order: 0,
        description: '',
        translations: []
    };
}
