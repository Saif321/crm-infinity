import { Component } from '@angular/core';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';

import { LevelService, LevelBase } from '../level.service';

@Component({
  selector: 'level-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends PageableComponent<LevelBase>  {
  constructor(
    protected levelService: LevelService,
    private interaction: InteractionService
  ) {
    super();
    this.load();
  }

  public async load() {
    const { data } = await this.levelService.search(
      this.searchQuery,
      this._page
    );
    this._list = data;
  }

  public async delete(id: number) {
    const isConfirmed = await this.interaction.confirm(
      'Are you sure you want to delete this level?'
    )
    if (isConfirmed) {
      await this.levelService.delete(id);
      this.load();
    }
  }
}
