import { Injectable } from '@angular/core';

import { FitApiClientService, FitList } from '@shared/fit-api-client.service';

interface LevelBase {
    id?: number;
    name: string;
    order: number;
    description: string;
}

interface LevelTranslation {
    language_id: number;
    name: string;
    description: string;
}

interface Level extends LevelBase {
    translations: LevelTranslation[];
}

export { FitList, LevelBase, Level, LevelTranslation };

@Injectable({ providedIn: 'root' })
export class LevelService {
    constructor(private fitApi: FitApiClientService) { }

    public search(query: string, page: number) {
        return this.fitApi.get<FitList<LevelBase>>('/admin/level', {
            search: query,
            page: page.toString()
        });
    }
    public get(id: number) {
        return this.fitApi.get<Level>('/admin/level/' + id);
    }
    public create(level: Level) {
        return this.fitApi.put<Level>('/admin/level/0', this.mapToRequest(level));
    }
    public update(level: Level) {
        return this, this.fitApi.put<Level>('/admin/level/' + level.id, this.mapToRequest(level));
    }
    public delete(id: number) {
        return this.fitApi.delete<any>('/admin/level/' + id);
    }

    private mapToRequest(level: Level): Level {
        return {
            name: level.name,
            description: level.description,
            order: level.order,
            translations: level.translations.filter(t => t.name && t.description)
                .map(t => {
                    return {
                        language_id: t.language_id,
                        name: t.name,
                        description: t.description
                    }
                })
        }
    }
}