import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { PaginationModule } from 'ngx-bootstrap/pagination';

import { SharedModule } from '@shared/shared.module';
import { GalleryModule } from '@app/gallery/gallery.module';
import { InteractionModule } from '@app/interaction/interaction.module';

import { BlogRoutingModule } from './blog-routing.module';
import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';
import { BlogService } from './blog.service';

@NgModule({
    declarations: [ListComponent, FormComponent],
    imports: [
        CommonModule,
        FormsModule,
        BlogRoutingModule,
        SharedModule,
        InteractionModule,
        CKEditorModule,
        PaginationModule.forRoot(),
        GalleryModule
    ],
    providers: [BlogService]
})
export class BlogModule {}
