import { Injectable } from '@angular/core';

import { FitApiClientService, FitList } from '@shared/fit-api-client.service';
import { GalleryItem } from '@app/gallery/gallery.service';

interface BlogBase {
    comment_count?: number;
    created_at?: string;
    description: string;
    id?: number;
    image_id: number;
    image?: GalleryItem;
    is_published?: boolean;
    like_count?: number;
    published_at?: string;
    review_count?: number;
    title: string;
    updated_at?: string;
}

interface BlogTranslation {
    language_id: number;
    title: string;
    description: string;
}

interface BlogListItem extends BlogBase { }

interface Blog extends BlogBase {
    translations: BlogTranslation[];
}

export { FitList, BlogListItem, Blog, BlogTranslation };

@Injectable()
export class BlogService {
    constructor(private fitApi: FitApiClientService) { }

    public search(query: string, page: number) {
        return this.fitApi.get<FitList<BlogListItem>>('/admin/blog', {
            search: query,
            page: page.toString()
        });
    }
    public get(id: number) {
        return this.fitApi.get<Blog>('/admin/blog/' + id);
    }

    public create(blog: Blog) {
        return this.fitApi.put<Blog>('/admin/blog/0', this.mapToRequest(blog));
    }

    public update(blog: Blog) {
        return this.fitApi.put<Blog>('/admin/blog/' + blog.id, this.mapToRequest(blog));
    }

    public delete(id: number) {
        return this.fitApi.delete<any>('/admin/blog/' + id);
    }

    public publish(blogId: number) {
        return this.fitApi.put<any>(`/admin/blog/${blogId}/publish`, null);
    }

    private mapToRequest(blog: Blog): Blog {
        return {
            title: blog.title,
            description: blog.description,
            image_id: blog.image_id,
            translations: blog.translations.filter(t => t.title && t.description)
                .map(t => {
                    return {
                        language_id: t.language_id,
                        title: t.title,
                        description: t.description
                    }
                })
        }
    }
}
