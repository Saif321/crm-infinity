import { Component, ViewEncapsulation } from '@angular/core';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';
import { CloudMessagingService } from '@shared/shared.service';

import { BlogService, BlogListItem } from '../blog.service';

@Component({
    selector: 'blog-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent extends PageableComponent<BlogListItem> {
    constructor(
        protected blogService: BlogService,
        protected cloudMessagingService: CloudMessagingService,
        private interaction: InteractionService
    ) {
        super();
        this.load();
    }

    public async publish(blog: BlogListItem) {
        let isConfirmed = await this.interaction.confirm(blog.is_published ? 'Are you sure you want to unpublish blog?' : 'Are you sure you want to publish blog?');
        if (isConfirmed) {
            await this.blogService.publish(blog.id);
            this.load();
        }
    }

    public async load() {
        const { data } = await this.blogService.search(
            this.searchQuery,
            this._page
        );
        this._list = data;
    }

    public async delete(id: number) {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to delete blog?'
        );
        if (isConfirmed) {
            await this.blogService.delete(id);
            this.load();
        }
    }
    public async notification() {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to push this notification?'
        );
        if (isConfirmed) {
        }
    }
    public async notify(id: number) {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to push this notification?'
        );
        if (isConfirmed) {
            await this.cloudMessagingService.notify({ object_id: id, object_type: 'Blog' });
        }
    }
}
