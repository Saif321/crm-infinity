import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as ClassicEditor from 'ckeditor5-build-classic-image-promise';

import { LanguagesComponent } from '@shared/components/languages/languages.component';
import { NotificationsService } from '@shared/notifications.service';
import { GalleryItem, GalleryService } from '@app/gallery/gallery.service';

import { BlogService, Blog, BlogTranslation } from '../blog.service';

@Component({
    selector: 'blog-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
    constructor(
        private blogs: BlogService,
        private route: ActivatedRoute,
        private router: Router,
        private notifications: NotificationsService,
        private gallery: GalleryService,
        private ngZone: NgZone
    ) { }

    public editor = ClassicEditor;
    public isDisabled = false;

    public config = {
        extraPlugins: [ClassicEditor],
        toolbar: [
            "undo",
            "redo",
            "|",
            "alignment",
            'fontSize',
            'fontColor',
            'fontBackgroundColor',
            'fontFamily',
            "bold",
            "italic",
            "blockQuote",
            "heading",
            "numberedList",
            "bulletedList",
            "|",
            "link",
            "mediaEmbed",
            "imagePromise"],
        image: {
            toolbar: [
                'imageStyle:full',
                'imageStyle:alignCenter',
                'imageStyle:alignLeft',
                'imageStyle:alignRight',
                '|',
                'imageTextAlternative'
            ],
            styles: [
                'full',
                'alignLeft',
                'alignCenter',
                'alignRight'
            ]
        },
        imagePromiseConfig: {
            getPromise: () => {
                return new Promise((resolve) => {
                    this.ngZone.run(() => {
                        this.gallery.selectFromGallery('image', 'gallery').then(res => {
                            resolve({
                                src: res.url
                            })
                        });
                    });
                });
            }
        }
    };

    public onEditorReady(editor) {
        console.log(editor.ui.componentFactory.names());
    }

    public selectedLanguageId = 1;

    public selectedGalleryItem: GalleryItem;

    @ViewChild(LanguagesComponent, { static: true })
    languagesComponent: LanguagesComponent;

    public isEditMode = false;

    public get isDefaultLanguage() {
        return this.languagesComponent.isDefaultLanguage;
    }

    public get blog() {
        return this._blog;
    }

    public get blogTranslation(): BlogTranslation {
        const blog = this._blog;
        if (!blog) {
            return;
        }
        let translation = blog.translations.find(
            t => t.language_id === this.selectedLanguageId
        );
        if (!translation) {
            translation = {
                title: blog.title,
                description: blog.description,
                language_id: this.selectedLanguageId
            };
            blog.translations.push(translation);
        }
        return translation;
    }

    public onBlogSubmit() {
        const t = this.getDefaultTranslation();
        this.blog.title = t.title;
        this.blog.description = t.description;
        if (this.isEditMode) {
            this.update();
        } else {
            this.create();
        }
    }

    public async ngOnInit() {
        this.route.paramMap.subscribe(params => {
            if (!params.has('id')) {
                this.isEditMode = false;
                return;
            }
            this.isEditMode = true;
            this.blogs.get(parseInt(params.get('id'))).then(res => {
                this._blog = res.data;
                this.selectedGalleryItem = res.data.image;
            });
        });
    }

    public onGalleryItemSelected($event: GalleryItem) {
        this._blog.image_id = $event ? $event.id : null;
    }

    private async create() {
        this.isDisabled = true;
        const { data, error } = await this.blogs.create(this.blog);
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'Create was successful!');
            this.router.navigate(['/blog/edit', data.id]);
        }
    }

    private async update() {
        this.isDisabled = true;
        const { data, error } = await this.blogs.update(this.blog);
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'The update was successful!');
        }
    }

    private getDefaultTranslation() {
        return this._blog.translations.find(t => t.language_id == this.languagesComponent.defaultLanguage.id);
    }

    private _blog: Blog = {
        comment_count: 0,
        description: '',
        image_id: null,
        is_published: false,
        like_count: 0,
        title: '',
        translations: [],
        review_count: 0
    };
}
