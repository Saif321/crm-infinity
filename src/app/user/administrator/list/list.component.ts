import { Component } from '@angular/core';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';

import { UserService, AdministratorListItem } from '../../user.service';

@Component({
  selector: 'admin-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends PageableComponent<AdministratorListItem>  {

  constructor(
    protected userService: UserService,
    private interaction: InteractionService
  ) {
    super();
    this.load();
  }

  public async load() {
    const { data } = await this.userService.searchAdministrator(
      this.searchQuery,
      this._page
    );
    this._list = data;
  }

  public async delete(id: number) {
    const isConfirmed = await this.interaction.confirm(
      'Are you sure you want to delete this administrator user?'
    );
    if (isConfirmed) {
      await this.userService.deleteAdministrator(id);
      this.load();
    }
  }
}
