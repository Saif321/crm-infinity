import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NotificationsService } from '@shared/notifications.service';

import { UserService, Administrator } from '../../user.service';
import { isStrongPassword } from '@app/auth/password-complexity';
import { GalleryItem } from '@app/gallery/gallery.service';

interface Gender {
    key: string;
    label: string;
}

const male = { key: 'male', label: 'Male' };
const female = { key: 'female', label: 'Female' };

@Component({
    selector: 'admin-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
    constructor(
        private admins: UserService,
        private route: ActivatedRoute,
        private router: Router,
        private notifications: NotificationsService
    ) { }

    public isEditMode = false;
    public isDisabled = false;
    public password = '';
    public passwordConfirm = '';

    public genderOptions: Gender[] = [male, female];
    public selectedGender: Gender;
    public selectedAvatar: GalleryItem;

    public get admin() {
        return this._admin;
    }

    public onAdminSubmit() {
        if (this.isEditMode) {
            this.update();
        } else {
            this.create();
        }
    }

    public onGallerySelected($event: GalleryItem) {
        this._admin.image_id = $event ? $event.id : null;
    }

    public async ngOnInit() {
        this.route.paramMap.subscribe(params => {
            if (!params.has('id')) {
                this.isEditMode = false;
                return;
            }
            this.isEditMode = true;
            this.admins.getAdministrator(parseInt(params.get('id'))).then(res => {
                this._admin = res.data;
                this.selectedGender = this._admin.gender === 'female' ? female : male;
                this.selectedAvatar = this._admin.image;
            });
        });
    }

    public async create() {
        if (this.password !== this.passwordConfirm) {
            this.notifications.warning('Password check', 'Password and confirm password do not match');
            return;
        }
        if (!isStrongPassword(this.password)) {
            this.notifications.warning('Password complexity', 'Password not complex enough');
            return;
        }
        this.isDisabled = true;
        const request = Object.assign({ password: this.password, gender: this.selectedGender.key }, this.admin);
        const { data, error } = await this.admins.createAdministrator(request);
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'Administrator created successfully!');
            this.router.navigate(['/user/administrator/edit', data.id]);
        }
    }

    public async update() {
        let request: any = Object.assign({}, this.admin);
        if (this.password) {
            if (this.password !== this.passwordConfirm) {
                this.notifications.warning('Password check', 'Password and confirm password do not match');
                return;
            }
            if (!isStrongPassword(this.password)) {
                this.notifications.warning('Password complexity', 'Password not complex enough');
                return;
            }
            request.password = this.password;
        }
        request.gender = this.selectedGender.key;
        this.isDisabled = true;
        const { data, error } = await this.admins.updateAdministrator(request);
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'The update was successful!');
        }
    }
    private _admin: Administrator = {
        date_of_birth: '',
        email: '',
        first_name: '',
        last_name: '',
        level_id: null,
        phone: '',
        username: '',
        roles: []
    };
}
