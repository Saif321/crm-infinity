import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListComponent as UserListComponent } from './user/list/list.component';
import { FormComponent as UserFormComponent } from './user/form/form.component';
import { ListComponent as AdministratorListComponent } from './administrator/list/list.component';
import { FormComponent as AdministratorFormComponent } from './administrator/form/form.component';

const routes: Routes = [
  { path: '', redirectTo: 'user' },
  { path: 'user', component: UserListComponent },
  // { path: 'user/create', component: UserFormComponent },
  // { path: 'user/edit/:id', component: UserFormComponent },
  { path: 'administrator', component: AdministratorListComponent },
  { path: 'administrator/create', component: AdministratorFormComponent },
  { path: 'administrator/edit/:id', component: AdministratorFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
