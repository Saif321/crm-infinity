import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NotificationsService } from '@shared/notifications.service';

import { UserService, User } from '../../user.service';

@Component({
  selector: 'user-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  constructor(
    private users: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private notifications: NotificationsService
  ) { }

  public isEditMode = false;

  public get user() {
    return this._user;
  }

  public onUserSubmit() {
    if (this.isEditMode) {
      this.update();
    } else {
      this.create();
    }
  }
  public async ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if (!params.has('id')) {
        this.isEditMode = false;
        return;
      }
      this.isEditMode = true;
      this.users.getUser(parseInt(params.get('id'))).then(res => {
        this._user = res.data;
      });
    });
  }

  public async create() {
    this._user.date_of_birth = '-';
    this._user.email = '-';
    this._user.first_name = '-';
    this._user.last_name = '-';
    this._user.level_id = 1;
    this._user.phone = '-';
    this._user.username = '-';

    const { data, error } = await this.users.createUser(this.user);
    this.router.navigate(['/user/user/edit', data.id]);
  }

  public async update() {
    const { data, error } = await this.users.updateUser(this.user);
    if (!error) {
      this.notifications.success('Success', 'The update was successful!');
    }
  }
  private _user: User = {
    date_of_birth: '',
    email: '',
    first_name: '',
    last_name: '',
    level_id: null,
    phone: '',
    username: ''
  };
}
