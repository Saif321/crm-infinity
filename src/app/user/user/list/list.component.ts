import { Component } from '@angular/core';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';

import { UserService, UserListItem, ProgramItemListResponse } from '../../user.service';
import { NutritionProgramBaseResponse } from '@app/nutrition-program/types';

@Component({
    selector: 'user-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent extends PageableComponent<UserListItem>  {

    constructor(
        protected userService: UserService,
        private interaction: InteractionService
    ) {
        super();
        this.load();
    }

    public userPrograms: ProgramItemListResponse[] = [];
    public userNutritionPrograms: NutritionProgramBaseResponse[] = [];
    public loadingUserPrograms = false;
    public loadingUserNutritionPrograms = false;

    public mapGoals(user: UserListItem) {
        if (!user.questionnaire || !user.questionnaire.goals) {
            return '';
        }
        return user.questionnaire.goals.map(i => i.name).join(', ');
    }

    public async load() {
        const { data } = await this.userService.searchUser(
            this.searchQuery,
            this._page
        );
        this._list = data;
    }

    public async delete(id: number) {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to delete user?'
        );
        if (isConfirmed) {
            await this.userService.deleteUser(id);
            this.load();
        }
    }

    public async loadUserWorkoutPrograms(user: UserListItem) {
        this.userPrograms = [];
        this.loadingUserPrograms = true;
        const { data, error } = await this.userService.getUserPrograms(user.id);
        this.loadingUserPrograms = false;
        if (!error) {
            this.userPrograms = data;
        }
    }
    public async loadUserNutritionPrograms(user: UserListItem) {
        this.userNutritionPrograms = [];
        this.loadingUserNutritionPrograms = true;
        const { data, error } = await this.userService.getUserNutritionPrograms(user.id);
        this.loadingUserNutritionPrograms = false;
        if (!error) {
            this.userNutritionPrograms = data;
        }
    }
}
