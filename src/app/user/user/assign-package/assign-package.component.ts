import { Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';

import { NotificationsService } from '@shared/notifications.service';

import { UserService, User } from '@app/user/user.service';
import { PopoverDirective } from 'ngx-bootstrap/popover';

@Component({
    selector: 'assign-package',
    templateUrl: './assign-package.component.html',
    styleUrls: ['./assign-package.component.scss']
})
export class AssignPackageComponent {
    constructor(
        protected userService: UserService,
        private notification: NotificationsService
    ) { }

    @Input() public user: User;
    @Output() public popoverOpen = new EventEmitter<AssignPackageComponent>(true);

    @ViewChild(PopoverDirective)
    public pop: PopoverDirective;

    public package: 'Monthly' | 'Yearly';

    public async assignPackageToUser(pop: PopoverDirective) {
        if (!this.package) {
            return;
        }
        const { data, error } = await this.userService.assignPackageToUser(this.user.id, this.package == 'Monthly' ? 1 : 2, null);
        if (!error) {
            this.notification
                .success('Success', this.package + ' package assigned successfuly to ' + this.user.first_name + ' ' + this.user.last_name);
            pop.hide();
        }
    }

    public close() {
        this.pop.hide();
    }

    public onOpen() {
        this.popoverOpen.emit(this);
    }
}
