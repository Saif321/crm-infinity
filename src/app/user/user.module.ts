import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PopoverModule } from 'ngx-bootstrap/popover';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgSelectModule } from '@ng-select/ng-select';

import { SharedModule } from '@shared/shared.module';
import { InteractionModule } from '@app/interaction/interaction.module';
import { GalleryModule } from '@app/gallery/gallery.module';

import { UserRoutingModule } from './user-routing.module';
import { AssignPackageComponent } from './user/assign-package/assign-package.component';
import { ListComponent as UserListComponent } from './user/list/list.component';
import { FormComponent as UserFormComponent } from './user/form/form.component';
import { ListComponent as AdministratorListComponent } from './administrator/list/list.component';
import { FormComponent as AdministratorFormComponent } from './administrator/form/form.component';

import { UserService } from './user.service'

@NgModule({
    declarations: [
        AssignPackageComponent,
        UserListComponent,
        UserFormComponent,
        AdministratorListComponent,
        AdministratorFormComponent
    ],
    imports: [
        CommonModule,
        UserRoutingModule,
        CommonModule,
        FormsModule,
        SharedModule,
        InteractionModule,
        PaginationModule.forRoot(),
        PopoverModule.forRoot(),
        GalleryModule,
        NgSelectModule
    ],
    providers: [UserService]
})

export class UserModule { }
