import { Injectable } from '@angular/core';

import { FitApiClientService, FitList } from '@shared/fit-api-client.service';
import { GalleryItem } from '@app/gallery/gallery.service';

import { ProgramItemListResponse } from '@app/program/types';

export { ProgramItemListResponse };

interface User {
    id?: number;
    first_name: string;
    last_name: string;
    username: string;
    email: string;
    phone: string;
    email_verified_at?: string;
    date_of_birth: string;
    gender?: 'male' | 'female';
    apple_id?: number;
    google_id?: number;
    is_active?: number;
    updated_at?: string;
    created_at?: string;
    image?: GalleryItem;
    level_id: number;
    image_id?: number;
    is_survey_completed?: number;
    nutrition_questionnaire?: NutritionQuestionnaire;
    questionnaire?: Questionnaire;
}

interface Goal {
    id: number;
    name: string;
    description: string;
    order: number;
}

interface Questionnaire {
    created_at: string;
    environment: Environment;
    exercise_environment_id: number;
    goals: Goal[];
    height_in_centimeters: number;
    id: number;
    updated_at: string;
    user_id: number;
    weight_in_kilograms: number;
}

interface Environment {
    created_at: string;
    id: number;
    name: string;
    updated_at: string;
}

interface NutritionQuestionnaire {
    created_at: string;
    id: number;
    is_fruits_like: number;
    is_meat_like: number;
    is_veggie_like: number;
    mine_description: string;
    typical_day: string;
    updated_at: string;
    user_id: number;
}

interface Role {
    created_at: string;
    guard_name: string;
    id: number;
    name: string;
    updated_at: string;
}

interface Administrator extends User {
    roles: Role[];
}

interface UserListItem extends User { }
interface AdministratorListItem extends Administrator { }

export { FitList, UserListItem, AdministratorListItem, User, Administrator };

@Injectable()
export class UserService {
    constructor(private fitApi: FitApiClientService) { }

    public searchAdministrator(query: string, page: number) {
        return this.fitApi.get<FitList<AdministratorListItem>>('/admin/administrator', {
            search: query,
            page: page.toString()
        });
    }

    public getAdministrator(id: number) {
        return this.fitApi.get<Administrator>('/admin/administrator/' + id);
    }

    public createAdministrator(admin: Administrator) {
        return this.fitApi.put<Administrator>('/admin/administrator/0', admin);
    }

    public updateAdministrator(admin: Administrator) {
        return this.fitApi.put<Administrator>('/admin/administrator/' + admin.id, admin);
    }

    public deleteAdministrator(id: number) {
        return this.fitApi.delete<any>('/admin/administrator/' + id);
    }

    public searchUser(query: string, page: number) {
        return this.fitApi.get<FitList<UserListItem>>('/admin/customer', {
            search: query,
            page: page.toString()
        });
    }

    public getUser(id: number) {
        return this.fitApi.get<User>('/admin/customer/' + id);
    }

    public createUser(user: User) {
        return this.fitApi.put<User>('/admin/customer/0', user);
    }

    public updateUser(user: User) {
        return this.fitApi.put<User>('/admin/customer/' + user.id, user);
    }

    public assignPackageToUser(userId: number, packageId: number, validUntil: string) {
        return this.fitApi.post<User>('/admin/package/customer/', {
            user_id: userId,
            package_id: packageId,
            valid_until: null
        });
    }

    public deleteUser(id: number) {
        return this.fitApi.delete<any>('/admin/customer/' + id);
    }

    public getUserPrograms(userId: number) {
        return this.fitApi.get<ProgramItemListResponse[]>(`/admin/customer/${userId}/programs`);
    }
    public getUserNutritionPrograms(userId: number) {
        return this.fitApi.get<any[]>(`/admin/customer/${userId}/nutrition-programs`);
    }
}
