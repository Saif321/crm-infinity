import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PaginationModule } from 'ngx-bootstrap/pagination';

import { SharedModule } from '@shared/shared.module';
import { ExerciseModule } from '@app/exercise/exercise.module';

import { WorkoutRoutingModule } from './workout-routing.module';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { WorkoutService } from './workout.service';
import { ExerciseItemComponent } from './round/exercise-item/exercise-item.component';
import { PauseItemComponent } from './round/pause-item/pause-item.component';
import { SetComponent } from './set/set.component';
import { ModalComponent } from './modal/modal/modal.component';
import { ModalBodyComponent } from './modal/modal-body/modal-body.component';


@NgModule({
    declarations: [
        FormComponent,
        ListComponent,
        ExerciseItemComponent,
        PauseItemComponent,
        SetComponent,
        ModalComponent,
        ModalBodyComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        WorkoutRoutingModule,
        SharedModule,
        ExerciseModule,
        DragDropModule,
        PaginationModule
    ],
    exports: [FormComponent],
    providers: [
        WorkoutService
    ]
})
export class WorkoutModule { }
