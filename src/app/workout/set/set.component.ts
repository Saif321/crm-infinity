
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { ControlContainer, NgForm } from '@angular/forms';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { InteractionService } from '@app/interaction/interaction.service';

import { ExerciseService } from '@app/exercise/exercise.service';

import { ExerciseSet, ExerciseSetItem } from '../types';

@Component({
    selector: 'app-set',
    templateUrl: './set.component.html',
    styleUrls: ['./set.component.scss'],
    viewProviders: [{ provide: ControlContainer, useExisting: NgForm }]
})
export class SetComponent {
    constructor(
        private exerciseService: ExerciseService,
        public form: NgForm,
        private interaction: InteractionService) { }

    @Input()
    public get set() { return this._set; }
    public set set(set) {
        this._set = set;
        if (!set.id) {
            this.isCollapsed = false;
        }
    }

    @Input() public setIndex: number;
    @Input() public selectedLanguageId = 1;
    @Input() public isDefaultLanguage = false;

    @Output() public deleteClick = new EventEmitter<ExerciseSet>(true);

    public isCollapsed = true;

    public get setTranslation() {
        const set = this.set;
        if (!set) {
            return;
        }
        let translation = set.translations.find(
            t => t.language_id === this.selectedLanguageId
        );
        if (!translation) {
            translation = {
                name: set.name,
                description: set.description,
                language_id: this.selectedLanguageId
            };
            set.translations.push(translation);
        }
        return translation;
    }

    public async addExercise(set: ExerciseSet) {
        const exercise = await this.exerciseService.selectFromExercise();
        if (!exercise) {
            return;
        }
        let item: ExerciseSetItem = {
            durationInSeconds: 0,
            type: 'exercise',
            exercise_id: exercise.id,
            numberOfReps: 0,
            strengthInPercentage: 0,
            exercise: exercise
        };

        set.items.push(item);
    }

    public onRoundItemDeleteClick(set: ExerciseSet, item: ExerciseSetItem) {
        set.items.splice(set.items.indexOf(item), 1);
    }

    public addPause(set: ExerciseSet, itemAfter: ExerciseSetItem) {
        let item: ExerciseSetItem = {
            durationInSeconds: 0,
            type: 'pause',
            exercise_id: 0,
            numberOfReps: 0,
            strengthInPercentage: 0
        };

        set.items.splice(set.items.indexOf(itemAfter) + 1, 0, item);
    }

    public onRoundItemDrop(event: CdkDragDrop<string[]>) {
        moveItemInArray(this.set.items, event.previousIndex, event.currentIndex);
    }

    public async onDeleteClick() {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to delete set?'
        )
        if (isConfirmed) {
            this.deleteClick.emit(this.set);
        }
    }

    private _set: ExerciseSet;
}
