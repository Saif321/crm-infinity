import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { LanguagesComponent } from '@shared/components/languages/languages.component';
import { NotificationsService } from '@shared/notifications.service';

import { WorkoutService } from '../workout.service';
import { Workout, ExerciseSet } from '../types';

@Component({
    selector: 'workout-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
    constructor(
        private workoutService: WorkoutService,
        private route: ActivatedRoute,
        private router: Router,
        private notifications: NotificationsService) { }

    @Output() cancelClick = new EventEmitter();
    @Output() updateWorkoutFinished = new EventEmitter<Workout>();

    public selectedLanguageId = 1;
    public isDisabled = false;

    @ViewChild(LanguagesComponent, { static: true })
    public languagesComponent: LanguagesComponent;

    public isEditMode = false;
    public isModalMode = false;

    public get isDefaultLanguage() {
        return this.languagesComponent.isDefaultLanguage;
    }

    public get workout() { return this._workout; }
    public get workoutTranslation() {
        const workout = this._workout;
        if (!workout) {
            return;
        }
        let translation = workout.translations.find(
            t => t.language_id === this.selectedLanguageId
        );
        if (!translation) {
            translation = {
                name: workout.name,
                description: workout.description,
                language_id: this.selectedLanguageId
            };
            workout.translations.push(translation);
        }
        return translation;
    }

    public ngOnInit() {
        this.route.paramMap.subscribe(params => {
            if (this.isModalMode) {
                return;
            }
            if (!params.has('id')) {
                this.isEditMode = false;
                return;
            }
            this.startEditing(parseInt(params.get('id')));
        });
    }

    public startEditing(id: number, modalMode = false) {
        this.isEditMode = true;
        this.isModalMode = modalMode;
        this.workoutService.get(id).then(res => {
            if (!res.error) {
                this._workout = this.workoutService.trainingResponseToWorkout(res.data);
            }
        });
    }

    public async onWorkoutSubmit() {
        if (this.isEditMode) {
            this.update();
        } else {
            this.create();
        }
    }

    public async create() {
        this.isDisabled = true;
        try {
            const request = this.workoutService.makeTrainingRequest(this._workout, this.languagesComponent.defaultLanguage.id, this.isModalMode);
            const { data, error } = await this.workoutService.create(request);
            this.isDisabled = false;
            if (!error) {
                this.notifications.success('Success', 'Create was successful!');
                this.router.navigate(['/workout/edit', data.id]);
            }
        } catch (error) {
            this.isDisabled = false;
            this.notifications.error('Error', 'The SET TYPE property is time and can not contain PAUSE. ');
        }
    }

    public async update() {
        this.isDisabled = true;
        try {
            const request = this.workoutService.makeTrainingRequest(this._workout, this.languagesComponent.defaultLanguage.id, this.isModalMode);
            const { data, error } = await this.workoutService.update(request);
            this.isDisabled = false;
            if (!error) {
                this.notifications.success('Success', 'The update was successful!');
                if (this.isModalMode) {
                    this._workout.id = data.id;
                    this._workout.parent_id = data.parent_id;
                    this._workout.name = data.name;
                    this.updateWorkoutFinished.emit(this._workout);
                }
            }
        } catch (error) {
            this.isDisabled = false;
            this.notifications.error('Error', 'The SET TYPE property is time and can not contain PAUSE. ');
        }
    }

    public addSet() {
        this._workout.exerciseSets.push({
            name: '',
            description: '',
            type: 'round',
            durationInSeconds: 0,
            translations: [],
            items: [],
            numberOfRounds: 0
        });
    }

    public onSetDrop(event: CdkDragDrop<string[]>) {
        moveItemInArray(this._workout.exerciseSets, event.previousIndex, event.currentIndex);
    }

    public onSetDeleteClick(sets: ExerciseSet[], item: ExerciseSet) {
        sets.splice(sets.indexOf(item), 1);
    }

    private _workout: Workout = {
        name: '',
        description: '',
        numberOfCalories: 0,
        exerciseSets: [],
        translations: []
    };
}
