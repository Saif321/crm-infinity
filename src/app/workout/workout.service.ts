import { Injectable } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { Exercise } from '@app/exercise/exercise.service';
import { FitApiClientService, FitList } from '@shared/fit-api-client.service';

import { Workout, ExerciseSet, ExerciseSetItem } from './types';
import { ModalComponent } from './modal/modal/modal.component';

export { Workout };

export interface ExerciseRoundItemRequest {
    type: 'pause' | 'exercise';
    duration_in_seconds: number;
    strength_percentage: number;
    number_of_reps: number;
    order: number;
    exercise_id: number;
}

export interface RoundRequest {
    order: number;
    items: ExerciseRoundItemRequest[];
}

export interface ExerciseSetTranslationRequest {
    name: string;
    description: string;
    language_id: number;
}

export interface ExerciseSetRequest {
    name: string;
    description: string;
    type: 'round' | 'time';
    duration_in_seconds: number;
    order: number;
    rounds: RoundRequest[];
    translations: ExerciseSetTranslationRequest[];
}

export interface TrainingTranslationRequest {
    name: string;
    description: string;
    language_id: number;
}

export interface TrainingRequest {
    id?: number;
    parent_id?: number;
    name: string;
    description: string;
    number_of_calories: number;
    exercise_sets: ExerciseSetRequest[];
    translations: TrainingTranslationRequest[];
}

interface RoundItemResponse {
    created_at: string;
    duration_in_seconds: number;
    exercise: Exercise
    exercise_id: number;
    id: number;
    number_of_reps: number;
    order: number;
    round_id: number;
    strength_percentage: number;
    type: 'pause' | 'exercise';
    updated_at: string;
}

interface RoundResponse {
    created_at: string;
    exercise_set_id: number;
    id: number;
    order: number;
    updated_at: string;
    items: RoundItemResponse[];
}

interface ExerciseSetResponseTranslation {
    description: string;
    id: number;
    name: string;
    language_id: number;
}

interface ExerciseSetResponse {
    created_at: string;
    description: string;
    duration_in_seconds: number;
    id: number;
    name: string;
    order: number;
    training_id: number;
    type: 'round' | 'time';
    updated_at: string;
    rounds: RoundResponse[];
    translations: ExerciseSetResponseTranslation[];
}

interface TrainingTranslationResponse {
    description: "Description"
    language_id: number;
    name: "Workout name"
}

export interface TrainingResponse {
    id: number;
    parent_id: number;
    name: string;
    description: string;
    number_of_calories: number;
    updated_at: number;
    created_at: number;
    translations: TrainingTranslationResponse[];
    exercise_sets: ExerciseSetResponse[];
}

@Injectable()
export class WorkoutService {
    constructor(private fitApi: FitApiClientService,
        private modalService: BsModalService) { }

    private bsModalRef: BsModalRef;
    public search(query: string, page: number, goal_ids?: number[], level_ids?: number[], environment_ids?: number[], is_published?: boolean) {
        var params: any = {
            search: query,
            page: page.toString()
        }
        if (is_published === true || is_published === false) {
            params.is_published = is_published === true ? 'true' : 'false'
        }
        if (Array.isArray(goal_ids) && goal_ids.length > 0) {
            params.goal_ids = goal_ids.join(',');
        }
        if (Array.isArray(level_ids) && level_ids.length > 0) {
            params.muscle_group_ids = level_ids.join(',');
        }
        if (Array.isArray(environment_ids) && environment_ids.length > 0) {
            params.environment_ids = environment_ids.join(',');
        }
        return this.fitApi.get<FitList<TrainingResponse>>('/admin/workout', params);
    }

    public get(id: number) {
        return this.fitApi.get<TrainingResponse>('/admin/workout/' + id);
    }

    public create(workout: TrainingRequest) {
        return this.fitApi.put<TrainingRequest>('/admin/workout/0', workout);
    }

    public update(workout: TrainingRequest) {
        return this.fitApi.put<TrainingRequest>('/admin/workout/' + workout.id, workout);
    }

    public delete(id: number) {
        return this.fitApi.delete<TrainingRequest>('/admin/workout/' + id);
    }

    public makeTrainingRequest(workout: Workout, defaultTranslationId: number, childTraining: boolean) {
        const mapSetItems = (items: ExerciseSetItem[]): ExerciseRoundItemRequest[] => {
            return items.map((item, index): ExerciseRoundItemRequest => {
                return {
                    duration_in_seconds: item.durationInSeconds,
                    exercise_id: item.exercise_id,
                    number_of_reps: item.numberOfReps,
                    order: index,
                    strength_percentage: item.strengthInPercentage,
                    type: item.type
                }
            });
        };

        const mapSetToRounds = (set: ExerciseSet): RoundRequest[] => {
            if (set.type === 'time') {
                return [{
                    order: 0,
                    items: mapSetItems(set.items)
                }];
            }

            return Array(set.numberOfRounds * 1).fill(1).map((_, index): RoundRequest => {
                return {
                    items: mapSetItems(set.items),
                    order: index
                }
            })
        };

        const defaultWorkoutTranslation = workout.translations.find(t => t.language_id == defaultTranslationId);

        const trainingRequest: TrainingRequest = {
            id: childTraining && !workout.parent_id ? 0 : workout.id,
            parent_id: childTraining && !workout.parent_id ? workout.id : workout.parent_id,
            name: defaultWorkoutTranslation.name,
            description: defaultWorkoutTranslation.description,
            number_of_calories: workout.numberOfCalories,
            exercise_sets: workout.exerciseSets.map((set, index): ExerciseSetRequest => {
                if (set.type === 'time' && set.items.some(i => i.type === 'pause')) {
                    throw new Error;
                }
                const defaultSetTranslation = set.translations.find(t => t.language_id == defaultTranslationId);
                return {
                    name: defaultSetTranslation.name,
                    description: defaultSetTranslation.description,
                    duration_in_seconds: set.durationInSeconds * 60,
                    order: index,
                    type: set.type,
                    rounds: mapSetToRounds(set),
                    translations: set.translations.filter(t => !!t.name).map(t => {
                        return {
                            name: t.name,
                            description: t.description,
                            language_id: t.language_id
                        }
                    })
                };
            }),
            translations: workout.translations.filter(t => !!t.name).map(t => {
                return {
                    name: t.name,
                    description: t.description,
                    language_id: t.language_id
                }
            })
        }

        return trainingRequest;
    }

    public trainingResponseToWorkout(response: TrainingResponse): Workout {
        return {
            id: response.id,
            parent_id: response.parent_id,
            name: response.name,
            description: response.description,
            numberOfCalories: response.number_of_calories,
            exerciseSets: response.exercise_sets.map((set): ExerciseSet => {
                return {
                    id: set.id,
                    name: set.name,
                    durationInSeconds: set.duration_in_seconds / 60,
                    description: set.description,
                    numberOfRounds: set.rounds.length,
                    items: set.rounds[0] ? set.rounds[0].items.map((i): ExerciseSetItem => {
                        return {
                            durationInSeconds: i.duration_in_seconds,
                            exercise: i.exercise,
                            exercise_id: i.exercise_id,
                            numberOfReps: i.number_of_reps,
                            strengthInPercentage: i.strength_percentage,
                            type: i.type
                        }
                    }) : [],
                    translations: set.translations,
                    type: set.type
                }
            }),
            translations: response.translations
        };
    }

    public selectFromWorkout(): Promise<TrainingResponse> {
        const promise = new Promise<TrainingResponse>((resolve, reject) => {
            const initialState = {
                onClose: (item: TrainingResponse) => {
                    resolve(item);
                },
            };
            this.bsModalRef = this.modalService.show(ModalComponent, { initialState, backdrop: false, ignoreBackdropClick: true, class: 'exercise-modal' });
        })
        return promise;
    }
}
