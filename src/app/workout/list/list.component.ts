import { Component, OnInit, EventEmitter } from '@angular/core';

import { InteractionService } from '@app/interaction/interaction.service';
import { PageableComponent } from '@shared/pageable.component';

import { TrainingRequest, WorkoutService } from '../workout.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent extends PageableComponent<TrainingRequest> {
    constructor(private workoutService: WorkoutService,
        private interaction: InteractionService) {
        super();
        this.load();
    }

    public showFilters = false;

    public async load() {
        const { data } = await this.workoutService.search(
            this.searchQuery,
            this._page
        );
        this._list = data;
    }

    public async delete(id: number) {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to delete workout?'
        )
        if (isConfirmed) {
            await this.workoutService.delete(id);
            this.load();
        }
    }
}
