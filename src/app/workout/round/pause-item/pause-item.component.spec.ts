import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PauseItemComponent } from './pause-item.component';

describe('PauseItemComponent', () => {
  let component: PauseItemComponent;
  let fixture: ComponentFixture<PauseItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PauseItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PauseItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
