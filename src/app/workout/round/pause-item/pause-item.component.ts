import { Component, Input, EventEmitter, Output } from '@angular/core';
import { ExerciseSetItem } from '../../types';

@Component({
    selector: 'app-pause-item',
    templateUrl: './pause-item.component.html',
    styleUrls: ['./pause-item.component.scss']
})
export class PauseItemComponent {
    @Input() public setIndex: number;
    @Input() public itemIndex: number;
    @Input() public item: ExerciseSetItem;
    @Input() public isDefaultLanguage = false;

    @Output() public deleteClick = new EventEmitter<ExerciseSetItem>(true);

    public onDeleteClick() {
        this.deleteClick.emit(this.item);
    }

}
