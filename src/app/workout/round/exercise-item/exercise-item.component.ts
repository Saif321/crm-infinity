import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ExerciseSetItem } from '../../types';

@Component({
    selector: 'app-exercise-item',
    templateUrl: './exercise-item.component.html',
    styleUrls: ['./exercise-item.component.scss']
})
export class ExerciseItemComponent {
    @Input() public setIndex: number;
    @Input() public itemIndex: number;
    @Input() public item: ExerciseSetItem;
    @Input() public isDefaultLanguage = false;

    @Output() public pauseClick = new EventEmitter<ExerciseSetItem>(true);
    @Output() public deleteClick = new EventEmitter<ExerciseSetItem>(true);

    public onDeleteClick() {
        this.deleteClick.emit(this.item);
    }

    public onPauseClick() {
        this.pauseClick.emit(this.item);
    }
}
