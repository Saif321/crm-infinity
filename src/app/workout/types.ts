import { ExerciseBase } from '@app/exercise/exercise.service';

export interface ExerciseSetItem {
    type: 'pause' | 'exercise';
    durationInSeconds: number;
    strengthInPercentage: number;
    numberOfReps: number;
    exercise_id: number;
    exercise?: ExerciseBase;
}

export interface ExerciseSet {
    id?: number;
    name: string;
    description?: string;
    type: 'round' | 'time';
    durationInSeconds: number;
    items: ExerciseSetItem[];
    translations: ExerciseSetTranslation[];
    numberOfRounds: number;
}

export interface ExerciseSetTranslation {
    name: string;
    description: string;
    language_id: number;
}

export interface WorkoutTranslation {
    name: string;
    description: string;
    language_id: number;
}

export interface Workout {
    id?: number;
    parent_id?: number;
    name: string;
    description: string;
    numberOfCalories: number;
    exerciseSets: ExerciseSet[];
    translations: WorkoutTranslation[];
}