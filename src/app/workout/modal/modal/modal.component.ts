import { Component } from '@angular/core';

import { TrainingResponse } from '../../workout.service';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
    selector: 'exercise-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
    constructor(
        public bsModalRef: BsModalRef
    ) { }

    public touched = false;
    public isDisabled = false;

    public select(item: TrainingResponse) {
        this.onClose(item);
        this.bsModalRef.hide();
    }

    private onClose(item: TrainingResponse) { }
}
