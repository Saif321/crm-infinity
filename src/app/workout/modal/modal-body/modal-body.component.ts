import { Component, Output, EventEmitter, Input } from '@angular/core';
import { PageableComponent } from '@shared/pageable.component';

import { WorkoutService, TrainingResponse } from '../../workout.service';

@Component({
    selector: 'modal-body',
    templateUrl: './modal-body.component.html',
    styleUrls: ['./modal-body.component.scss']
})
export class ModalBodyComponent extends PageableComponent<TrainingResponse> {

    constructor(
        protected workoutService: WorkoutService
    ) {
        super();
        this.load();
    }

    @Output() public itemSelected = new EventEmitter<TrainingResponse>(true);

    @Input() public selected: TrainingResponse;

    public async load() {
        const { data } = await this.workoutService.search(
            this.searchQuery,
            this._page,
            undefined,
            undefined,
            undefined,
            true
        );
        this._list = data;
    }

    public select(item: TrainingResponse) {
        this.selected = item;
        this.itemSelected.emit(item);
    }

}
