import { Injectable } from '@angular/core';

import { FitApiClientService, FitList } from '@shared/fit-api-client.service';

interface MuscleGroupBase {
    id?: number;
    name: string;
    updated_at?: string;
    created_at?: string;
}
interface MuscleGroupTranslation {
    language_id: number;
    name: string;
}
interface MuscleGroup extends MuscleGroupBase {
    slug?: string;
    translations: MuscleGroupTranslation[];
}

export { FitList, MuscleGroupBase, MuscleGroup, MuscleGroupTranslation };

@Injectable({ providedIn: 'root' })
export class MuscleGroupService {
    constructor(private fitApi: FitApiClientService) { }

    public search(query: string, page: number) {
        return this.fitApi.get<FitList<MuscleGroup>>('/admin/muscle-group', {
            search: query,
            page: page.toString()
        });
    }
    public get(id: number) {
        return this.fitApi.get<MuscleGroup>('/admin/muscle-group/' + id);
    }

    public create(muscleGroup: MuscleGroup) {
        return this.fitApi.put<MuscleGroup>('/admin/muscle-group/0', muscleGroup);
    }

    public update(muscleGroup: MuscleGroup) {
        return this, this.fitApi.put<MuscleGroup>('/admin/muscle-group/' + muscleGroup.id, muscleGroup);
    }

    public delete(id: number) {
        return this.fitApi.delete<any>('/admin/muscle-group/' + id);
    }
}
