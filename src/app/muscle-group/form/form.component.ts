import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';

import { MuscleGroupService, MuscleGroup, MuscleGroupTranslation } from '../muscle-group.service';
import { LanguagesComponent } from '@shared/components/languages/languages.component';
import { NotificationsService } from '@shared/notifications.service';

@Component({
    selector: 'muscle-group-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

    constructor(
        private muscleGroups: MuscleGroupService,
        private route: ActivatedRoute,
        private router: Router,
        private notifications: NotificationsService
    ) { }

    public selectedLanguageId = 1;
    public isDisabled = false;

    @ViewChild(LanguagesComponent, { static: true })
    languagesComponent: LanguagesComponent;

    public isEditMode = false;

    public get isDefaultLanguage() {
        return this.languagesComponent.isDefaultLanguage;
    }

    public get muscleGroup() {
        return this._muscleGroup;
    }

    public get muscleGroupTranslation(): MuscleGroupTranslation {
        const muscleGroup = this._muscleGroup;
        if (!muscleGroup) {
            return;
        }
        let translation = muscleGroup.translations.find(
            t => t.language_id === this.selectedLanguageId
        );
        if (!translation) {
            translation = {
                name: muscleGroup.name,
                language_id: this.selectedLanguageId
            };
            muscleGroup.translations.push(translation);
        }
        return translation;
    }

    public onMuscleGroupSubmit() {
        const defaultTranslation = this.getDefaultTranslation();
        this._muscleGroup.name = defaultTranslation.name;
        this._muscleGroup.slug = undefined;
        if (this.isEditMode) {
            this.update();
        } else {
            this.create();
        }
    }

    public async ngOnInit() {
        this.route.paramMap.subscribe(params => {
            if (!params.has('id')) {
                this.isEditMode = false;
                return;
            }
            this.isEditMode = true;
            this.muscleGroups.get(parseInt(params.get('id'))).then(res => {
                this._muscleGroup = res.data;
            });
        });
    }

    public async create() {
        this.isDisabled = true;
        const { data, error } = await this.muscleGroups.create(this.getRequest());
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'Create was successful!');
            this.router.navigate(['/muscle-group/edit', data.id]);
        }
    }

    public async update() {
        this.isDisabled = true;
        const { data, error } = await this.muscleGroups.update(this.getRequest());
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'The update was successful!');
        }
    }

    private getDefaultTranslation() {
        return this._muscleGroup.translations.find(t => t.language_id == this.languagesComponent.defaultLanguage.id);
    }

    private getRequest(): MuscleGroup {
        const defaultTranslation = this.getDefaultTranslation();
        return {
            id: this._muscleGroup.id,
            name: defaultTranslation.name,
            translations: this._muscleGroup.translations.filter(t => t.name)
                .map(t => {
                    return {
                        language_id: t.language_id,
                        name: t.name
                    }
                })
        }
    }

    private _muscleGroup: MuscleGroup = {
        name: '',
        translations: []
    };
}
