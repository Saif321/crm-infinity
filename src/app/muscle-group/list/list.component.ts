import { Component } from '@angular/core';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';

import { MuscleGroup, MuscleGroupService } from '../muscle-group.service';

@Component({
    selector: 'muscle-group-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent extends PageableComponent<MuscleGroup> {
    constructor(
        protected muscleGroupService: MuscleGroupService,
        private interaction: InteractionService
    ) {
        super();
        this.load();
    }

    public async load() {
        const { data } = await this.muscleGroupService.search(
            this.searchQuery,
            this._page
        );
        this._list = data;
    }

    public async delete(id: number) {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to delete muscle group?'
        )
        if (isConfirmed) {
            await this.muscleGroupService.delete(id);
            this.load();
        }
    }
}
