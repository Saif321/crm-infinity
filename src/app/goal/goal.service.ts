import { Injectable } from '@angular/core';

import { FitApiClientService, FitList } from '@shared/fit-api-client.service';

interface GoalBase {
    id?: number;
    name: string;
    order: number;
    description: string;
}

interface GoalTranslation {
    language_id: number;
    name: string;
    description: string;
}

interface Goal extends GoalBase {
    translations: GoalTranslation[];
}

export { FitList, GoalBase, Goal, GoalTranslation };

@Injectable({ providedIn: 'root' })
export class GoalService {
    constructor(private fitApi: FitApiClientService) { }

    public search(query: string, page: number) {
        return this.fitApi.get<FitList<GoalBase>>('/admin/goal', {
            search: query,
            page: page.toString()
        });
    }
    public get(id: number) {
        return this.fitApi.get<Goal>('/admin/goal/' + id);
    }
    public create(goal: Goal) {
        return this.fitApi.put<Goal>('/admin/goal/0', this.mapToRequest(goal));
    }
    public update(goal: Goal) {
        return this, this.fitApi.put<Goal>('/admin/goal/' + goal.id, this.mapToRequest(goal));
    }
    public delete(id: number) {
        return this.fitApi.delete<any>('/admin/goal/' + id);
    }

    private mapToRequest(goal: Goal): Goal {
        return {
            name: goal.name,
            description: goal.description,
            order: goal.order,
            translations: goal.translations.filter(t => t.name && t.description)
                .map(t => {
                    return {
                        language_id: t.language_id,
                        name: t.name,
                        description: t.description
                    }
                })
        }
    }
}