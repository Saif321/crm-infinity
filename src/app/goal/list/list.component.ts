import { Component } from '@angular/core';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';

import { GoalService, GoalBase } from '../goal.service';

@Component({
    selector: 'goal-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent extends PageableComponent<GoalBase>  {
    constructor(
        protected goalService: GoalService,
        private interaction: InteractionService
    ) {
        super();
        this.load();
    }

    public async load() {
        const { data } = await this.goalService.search(
            this.searchQuery,
            this._page
        );
        this._list = data;
    }

    public async delete(id: number) {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to delete this goal?'
        )
        if (isConfirmed) {
            await this.goalService.delete(id);
            this.load();
        }
    }
}
