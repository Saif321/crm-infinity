import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { LanguagesComponent } from '@shared/components/languages/languages.component';
import { NotificationsService } from '@shared/notifications.service';

import { GoalService, Goal, GoalTranslation } from '../goal.service';

@Component({
    selector: 'goal-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
    constructor(
        private goals: GoalService,
        private route: ActivatedRoute,
        private router: Router,
        private notifications: NotificationsService
    ) { }

    @ViewChild(LanguagesComponent, { static: true }) languagesComponent: LanguagesComponent;

    public selectedLanguageId = 1;
    public isEditMode = false;
    public isDisabled = false;

    public get isDefaultLanguage() {
        return this.languagesComponent.isDefaultLanguage;
    }

    public get goal() {
        return this._goal;
    }

    public get goalTranslation(): GoalTranslation {
        const goal = this._goal;
        if (!goal) {
            return;
        }
        let translation = goal.translations.find(
            t => t.language_id === this.selectedLanguageId
        );
        if (!translation) {
            translation = {
                name: goal.name,
                description: goal.description,
                language_id: this.selectedLanguageId
            };
            goal.translations.push(translation);
        }
        return translation;
    }

    public onGoalSubmit() {
        const defaultTranslation = this.getDefaultTranslation();
        this._goal.name = defaultTranslation.name;
        this._goal.description = defaultTranslation.description;
        if (this.isEditMode) {
            this.update();
        } else {
            this.create();
        }
    }

    public async ngOnInit() {
        this.route.paramMap.subscribe(params => {
            if (!params.has('id')) {
                this.isEditMode = false;
                return;
            }
            this.isEditMode = true;
            this.goals.get(parseInt(params.get('id'))).then(res => {
                this._goal = res.data;
            });
        });
    }

    public async create() {
        this.isDisabled = true;
        const { data, error } = await this.goals.create(this.goal);
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'Create was successful!');
            this.router.navigate(['/goal/edit', data.id]);
        }
    }

    public async update() {
        this.isDisabled = true;
        const { data, error } = await this.goals.update(this.goal);
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'The update was successful!');
        }
    }

    public getDefaultTranslation() {
        return this._goal.translations.find(t => t.language_id == this.languagesComponent.defaultLanguage.id);
    }

    private _goal: Goal = {
        name: '',
        order: null,
        description: '',
        translations: []
    };
}
