import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';

import { ReviewService, Comment, isBlogComment, isProgramComment, isNutritionProgramComment } from '../review.service';

interface BadgeOptions {
    class: string;
    label: string;
    editUrl: string;
}

const badgeMapOptions: { [key: string]: BadgeOptions } = {
    'App\\Models\\Program': {
        class: 'badge-gray',
        label: 'Program',
        editUrl: '/program/edit'
    },
    'App\\Models\\Blog': {
        class: 'badge-gray',
        label: 'Blog',
        editUrl: '/blog/edit'
    },
    'App\\Models\\NutritionProgram': {
        class: 'badge-gray',
        label: 'Nutrition',
        editUrl: '/nutrition-program/edit'
    },
    'App\\Models\\Comment': {
        class: 'badge-gray',
        label: 'Comment',
        editUrl: null
    }
};

@Component({
    selector: 'comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.scss']
})
export class CommentComponent extends PageableComponent<Comment> {
    constructor(
        protected reviewService: ReviewService,
        private interaction: InteractionService,
        private router: Router
    ) {
        super();
        this.load();
    }

    public async publishComment(comment: Comment) {
        let isConfirmed = await this.interaction.confirm(comment.is_published ? 'Are you sure you want to unpublish comment?' : 'Are you sure you want to publish comment?');
        if (isConfirmed) {
            await this.reviewService.publishComment(comment.id);
            this.load();
        }
    }

    public getBadgeOptions(comment: Comment) {
        return badgeMapOptions[comment.object_type];
    }

    public goTo(comment: Comment) {
        const badgeOption = this.getBadgeOptions(comment);
        if (!badgeOption.editUrl) {
            return;
        }
        let url = this.router.createUrlTree([badgeOption.editUrl, comment.object_id])
        window.open(url.toString(), '_blank')
    }

    public getBadgeName(comment: Comment): string {
        if (comment.root != null) {
            if (isBlogComment(comment.root)) {
                return comment.root.title;
            }
            if (isProgramComment(comment.root)) {
                return comment.root.name;
            }
            if (isNutritionProgramComment(comment.root)) {
                return comment.root.name;
            }
        }
    }

    public async load() {
        const { data } = await this.reviewService.searchComments(
            this.searchQuery,
            this._page
        );
        this._list = data;
    }
}
