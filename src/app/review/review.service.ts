import { Injectable } from '@angular/core';

import { FitApiClientService, FitList } from '@shared/fit-api-client.service';
import { NutritionProgramBaseResponse } from '@app/nutrition-program/types';
import { Blog } from '@app/blog/blog.service';

export type ProgramOrNutritionProgramOrBlog = Program | NutritionProgramBaseResponse | Blog;

interface Comment {
    comment: string;
    created_at?: string;
    id?: number;
    is_published: boolean;
    like_count: number;
    object_id: number;
    object_type: string;
    published_at?: string;
    root: ProgramOrNutritionProgramOrBlog;
    updated_at?: string;
    updated_id?: number;
    user: User;
    user_id: number;
}

interface Program {
    comment_count: number;
    created_at: null;
    id: number;
    image_id: number;
    is_published: boolean;
    level_id: number;
    like_count: number;
    name: string;
    review_count: number;
    updated_at: string;
    video_id: number;
    workout_days_per_week: number;
}

interface User {
    apple_id: number;
    created_at: string;
    email: string;
    email_verified_at: string;
    first_name: string;
    google_id: number;
    id: number;
    image: string;
    is_active: number;
    is_survey_completed: number;
    last_name: string;
    phone: string;
    updated_at: string;
    username: string;
}

interface Review {
    created_at?: string;
    description: string;
    id?: number;
    is_published: number;
    object_id: number;
    object_type: string;
    published_at?: string;
    rating: number;
    root: ProgramOrNutritionProgramOrBlog;
    updated_at?: string;
    user: User;
    user_id: number;
}

export const isBlogComment = (root: ProgramOrNutritionProgramOrBlog): root is Blog => {
    return !!(root as Blog).title;
};
export const isProgramComment = (root: ProgramOrNutritionProgramOrBlog): root is Program => {
    return !!(root as Program).name;
};
export const isNutritionProgramComment = (root: ProgramOrNutritionProgramOrBlog): root is NutritionProgramBaseResponse => {
    return !!(root as NutritionProgramBaseResponse).name;
};

export { FitList, Comment, Review };

@Injectable()
export class ReviewService {
    constructor(private fitApi: FitApiClientService) { }

    public searchComments(query: string, page: number) {
        return this.fitApi.get<FitList<Comment>>('/admin/comment', {
            search: query,
            page: page.toString()
        });
    }
    public get(id: number) {
        return this.fitApi.get<Comment>('/admin/comment/' + id);
    }
    public publishComment(commentId: number) {
        return this.fitApi.put<any>(`/admin/comment/${commentId}/publish`, null);
    }
    public searchReviews(query: string, page: number) {
        return this.fitApi.get<FitList<Review>>('/admin/review', {
            search: query,
            page: page.toString()
        });
    }
    public publishReview(reviewId: number) {
        return this.fitApi.put<any>(`/admin/review/${reviewId}/publish`, null);
    }
}
