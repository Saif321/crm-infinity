import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';

import { ReviewService, Review, isBlogComment, isProgramComment, isNutritionProgramComment } from '../review.service';

interface BadgeOptions {
    class: string;
    label: string;
    editUrl: string;
}

const badgeMapOptions: { [key: string]: BadgeOptions } = {
    'App\\Models\\Program': {
        class: 'badge-gray',
        label: 'Program',
        editUrl: '/program/edit'
    },
    'App\\Models\\Blog': {
        class: 'badge-gray',
        label: 'Blog',
        editUrl: '/blog/edit'
    },
    'App\\Models\\NutritionProgram': {
        class: 'badge-gray',
        label: 'Nutrition',
        editUrl: '/nutrition-program/edit'
    },
    'App\\Models\\Comment': {
        class: 'badge-gray',
        label: 'Comment',
        editUrl: null
    }
};

@Component({
    selector: 'review',
    templateUrl: './review.component.html',
    styleUrls: ['./review.component.scss']
})
export class ReviewComponent extends PageableComponent<Review> {
    constructor(
        protected reviewService: ReviewService,
        private interaction: InteractionService,
        private router: Router
    ) {
        super();
        this.load();
    }

    public getBadgeOptions(review: Review) {
        return badgeMapOptions[review.object_type];
    }

    public getBadgeName(review: Review): string {
        if (review.root != null) {
            if (isBlogComment(review.root)) {
                return review.root.title;
            }
            if (isProgramComment(review.root)) {
                return review.root.name;
            }
            if (isNutritionProgramComment(review.root)) {
                return review.root.name;
            }
        }
    }

    public goTo(review: Review) {
        const badgeOption = this.getBadgeOptions(review);
        if (!badgeOption.editUrl) {
            return;
        }
        let url = this.router.createUrlTree([badgeOption.editUrl, review.object_id])
        window.open(url.toString(), '_blank')
    }

    public async publishReview(review: Review) {
        let isConfirmed = await this.interaction.confirm(review.is_published == 1 ? 'Are you sure you want to unpublish this review?' : 'Are you sure you want to publish this review?');
        if (isConfirmed) {
            await this.reviewService.publishReview(review.id);
            this.load();

        }
    }

    public async load() {
        const { data } = await this.reviewService.searchReviews(
            this.searchQuery,
            this._page
        );
        this._list = data;
    }
}
