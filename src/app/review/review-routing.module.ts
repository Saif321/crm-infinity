import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommentComponent } from './comment/comment.component';
import { ReviewComponent } from './review/review.component';

const routes: Routes = [
    { path: '', redirectTo: 'comment' },
    { path: 'comment', component: CommentComponent },
    { path: 'review', component: ReviewComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReviewRoutingModule { }
