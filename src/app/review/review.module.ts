import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PaginationModule } from 'ngx-bootstrap/pagination';
import { PopoverModule } from 'ngx-bootstrap/popover';

import { SharedModule } from '@shared/shared.module';
import { InteractionModule } from '@app/interaction/interaction.module';

import { ReviewRoutingModule } from './review-routing.module';
import { CommentComponent } from './comment/comment.component';
import { ReviewService } from './review.service';
import { ReviewComponent } from './review/review.component';

@NgModule({
    declarations: [CommentComponent, ReviewComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReviewRoutingModule,
        SharedModule,
        InteractionModule,
        PaginationModule.forRoot(),
        PopoverModule.forRoot()
    ],
    providers: [ReviewService]
})
export class ReviewModule { }
