import { Injectable } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { FitApiClientService, FitList } from '@shared/fit-api-client.service';
import { GalleryModalComponent } from './gallery-modal/gallery-modal.component';

export interface GalleryItem {
    id: number;
    created_at: string;
    extension: string;
    mime_type: string;
    name: string;
    size: string;
    thumbnail: string;
    type: 'image' | 'video';
    updated_at: string;
    url: string;
    playMode: boolean;
}

export interface GalleryUploadLimits {
    image_types: string[];
    video_types: string[];
    upload_limit_in_bytes: number;
}

@Injectable()
export class GalleryService {
    constructor(private fitApi: FitApiClientService, private modalService: BsModalService) { }

    public search(query: string, page: number, type?: 'video' | 'image') {
        return this.fitApi.get<FitList<GalleryItem>>('/admin/gallery', {
            search: query,
            type: type,
            page: page.toString()
        });
    }

    public searchAvatar(query: string, page: number, type?: 'video' | 'image') {
        return this.fitApi.get<FitList<GalleryItem>>('/admin/gallery/avatar', {
            search: query,
            type: type,
            page: page.toString()
        });
    }

    public selectFromGallery(type: 'all' | 'image' | 'video', source: 'gallery' | 'avatar'): Promise<GalleryItem> {
        const promise = new Promise<GalleryItem>((resolve, reject) => {
            const initialState = {
                onClose: (item: GalleryItem) => {
                    resolve(item);
                },
                type: type,
                source: source
            };
            this.bsModalRef = this.modalService.show(GalleryModalComponent, { initialState, backdrop: false, ignoreBackdropClick: true, class: 'gallery-modal' });
        })
        return promise;
    }

    public delete(id: number) {
        return this.fitApi.delete<any>(`/admin/gallery/${id}`);
    }

    public update(item: GalleryItem) {
        return this.fitApi.put<GalleryItem>(`/admin/gallery/${item.id}`, item);
    }

    public upload(fileName: string, file: File, isUserAvatar: boolean) {
        const form = new FormData();
        form.append('url', file);
        form.append('name', fileName);
        form.append('is_user_avatar', isUserAvatar ? '1' : '0');
        return this.fitApi.uploadFileToGallery<GalleryItem>(form);
    }

    public getUploadLimits() {
        return this.fitApi.get<GalleryUploadLimits>('/admin/gallery/upload-limits');
    }

    private bsModalRef: BsModalRef;
}
