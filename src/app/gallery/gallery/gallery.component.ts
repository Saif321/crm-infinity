import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core';
import { PageableComponent } from '@shared/pageable.component';
import { NotificationsService } from '@shared/notifications.service';
import { InteractionService } from '@app/interaction/interaction.service';

import { GalleryService, GalleryItem, GalleryUploadLimits } from '../gallery.service';

var acceptedVideoMimeTypes = ['mp4', 'mpeg4', 'mpeg'];
var acceptedImageMimeTypes = ['jpeg', 'png'];

function formatBytes(bytes, decimals) {
    if (bytes == 0) return '0 Bytes';
    var k = 1024,
        dm = decimals <= 0 ? 0 : decimals || 2,
        sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
        i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

@Component({
    selector: 'app-gallery',
    templateUrl: './gallery.component.html',
    styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent extends PageableComponent<GalleryItem> implements OnInit {
    constructor(private galleryService: GalleryService, private interaction: InteractionService, private notification: NotificationsService) {
        super();
    }

    get maxUploadSize() { return formatBytes(this.uploadLimits.upload_limit_in_bytes, 2); }

    @Output() public itemSelected = new EventEmitter<GalleryItem>(true);

    @Input() public selected: GalleryItem;

    @Input() public type: 'all' | 'image' | 'video' = 'all';

    @Input() public source: 'gallery' | 'avatar' = 'gallery';

    public uploadLimits: GalleryUploadLimits = { image_types: [], video_types: [], upload_limit_in_bytes: 0 };

    public fakePath: string;

    public fileName: string;

    public progress = 0;

    public editMode = false;

    public switchPlayMode(item: GalleryItem) {
        item.playMode = !item.playMode;
    }

    public ngOnInit() {
        super.ngOnInit();
        this.galleryService.getUploadLimits()
            .then((res) => {
                this.uploadLimits = res.data;
                acceptedVideoMimeTypes = res.data.video_types;
                acceptedImageMimeTypes = res.data.image_types;
            });
        this.load();
    }

    public isEditing(item: GalleryItem): boolean {
        return this.itemsEditMap.has(item) && this.itemsEditMap.get(item);
    }

    public startEditing(item: GalleryItem) {
        this.itemsEditMap.forEach((value, item) => {
            this.itemsEditMap.set(item, false);
        })
        this.itemsEditMap.set(item, true);
    }

    public async stopEditing(item: GalleryItem) {
        const { error } = await this.galleryService.update(item);
        if (!error) {
            this.itemsEditMap.set(item, false);
        }
    }

    public async load() {
        const search = this.source === 'avatar' ? this.galleryService.searchAvatar : this.galleryService.search;
        const { data } = await search.call(
            this.galleryService,
            this.searchQuery,
            this._page,
            this.type !== 'all' ? this.type : undefined
        );
        this._list = data;
    }

    public async upload() {
        const res = await this.galleryService.upload(this.fileName, this.fileList.item(0), this.source === 'avatar');

            res.subscribe(
                res => {
                    if (res.progress) {
                        this.progress = res.progress;
                    } else if (res.response) {
                        this.progress = 0;
                        this.load();
                    } else {
                        this.progress = 0;
                    }
                },
                error => { this.progress = 0; },
                () => { this.fileName = ''; this.fileList = null; }
            )
    }

    public onFileChange(event) {
        this.fileList = event.target.files;
        const item = this.fileList.item(0);
        if (item) {
            const nameParts = item.name.split('.');
            nameParts.pop();
            this.fileName = nameParts.join('.');
            if (this.isValidFileType(item)) {
                this.upload();
            } else {
                this.notification.warning('Invalid file type', 'Acceptable file types are ' + this.getAcceptableFileTypes().join(', '));
            }
        }
    }

    public async delete(id: number) {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to delete file from gallery?'
        );
        if (isConfirmed) {
            await this.galleryService.delete(id);
            this.load();
        }
    }

    public select(item: GalleryItem) {
        this.selected = item;
        this.itemSelected.emit(item);
    }

    private isValidFileType(file: File) {
        var typeValidator = (types: string[]) => types.some(type => file.type.indexOf(type) > -1);
        return typeValidator(this.getAcceptableFileTypes());
    }

    private getAcceptableFileTypes() {
        switch (this.type) {
            case 'image':
                return acceptedImageMimeTypes;
            case 'video':
                return acceptedVideoMimeTypes;
            case 'all':
                return [...acceptedImageMimeTypes, ...acceptedImageMimeTypes];
        }
    }

    private fileList: FileList;
    private itemsEditMap = new Map<GalleryItem, boolean>();
}
