import { Component, Input, ViewChild, ContentChild, ElementRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, Validator, ValidationErrors, AbstractControl, NG_VALIDATORS } from '@angular/forms';

import { GalleryItem, GalleryService } from '../gallery.service';

@Component({
    selector: 'gallery-item',
    templateUrl: './gallery-item.component.html',
    styleUrls: ['./gallery-item.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: GalleryItemComponent, multi: true },
        { provide: NG_VALIDATORS, useExisting: GalleryItemComponent, multi: true }
    ]
})
export class GalleryItemComponent implements ControlValueAccessor, Validator {
    constructor(private gallery: GalleryService, private elementRef: ElementRef<HTMLDivElement>) { }

    get item() { return this._item; }

    @Input()
    get required() { return this._required; }
    set required(value) { this._required = value !== false; }

    @Input()
    get readonly() { return this._readonly; }
    set readonly(value) { this._readonly = value !== false; }

    @Input() public type: 'all' | 'image' | 'video' = 'all';

    @Input() public source: 'gallery' | 'avatar' = 'gallery';

    public touched = false;

    public isDisabled = false;

    public onValidatorChange: () => void = () => { };

    public onChange: (value: GalleryItem) => void = (value: GalleryItem) => { };

    public onTouched: () => void = () => { };

    public writeValue(value: GalleryItem): void {
        if (this.videoPlayer) {
            setTimeout(() => { this.videoPlayer.load(); });
        }
        this._item = value;
    }

    public switchPlayMode(item: GalleryItem) {
        item.playMode = !item.playMode;
    }

    public registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    public registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    public setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

    public validate(control: AbstractControl): ValidationErrors {
        if (this.required && !this._item) {
            return { galleryItemNotSelected: 'Gallery item is required' };
        }
        return null;
    }

    public registerOnValidatorChange(fn: () => void): void {
        this.onValidatorChange = fn;
    }

    public async openGallery() {
        this.onTouched();
        this._item = await this.gallery.selectFromGallery(this.type, this.source);
        this.onChange(this._item);
        this.onValidatorChange();
    }

    public onRemoveClick() {
        this._item = null;
        this.onChange(this._item);
    }

    private get videoPlayer(): HTMLVideoElement {
        return this.elementRef.nativeElement.querySelector('video');
    }

    private _item: GalleryItem;
    private _required = false;
    private _readonly = false;
}
