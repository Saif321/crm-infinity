import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthorizationService } from '@shared/authorization.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    constructor(private auth: AuthorizationService, private router: Router) { }
    get isAuthenticated() {
        return this.auth.isAuthenticated();
    }

    get user() { return this.auth.user; }

    public isCollapsed = false;

    public async logout() {
        await this.auth.logout();
    }
}
