import { TestBed, async, inject } from '@angular/core/testing';

import { AuthorizationNeededGuard } from './authorization-needed.guard';

describe('AuthorizationNeededGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthorizationNeededGuard]
    });
  });

  it('should ...', inject([AuthorizationNeededGuard], (guard: AuthorizationNeededGuard) => {
    expect(guard).toBeTruthy();
  }));
});
