import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

import { FitConfigProviderService } from './fit-config-provider.service';
import { FitApiClientService } from './fit-api-client.service';
import { FitAppConfig } from './fit-app-config';

interface User {
    created_at: string;
    email: string;
    email_verified_at: string;
    id: number;
    is_active: number;
    first_name: string;
    last_name: string;
    phone: string;
    updated_at: string;
    username: string;
    image: {
        url: string;
    }
}

interface TokenResponse {
    token_type: string;
    expires_in: number;
    access_token: string;
    refresh_token: string;
}

interface LogoutResponse {
    success: boolean;
    errorCode: number;
    errorMessage: string;
    data: {
        message: string;
    };
}

export function tokenClear() {
    return localStorage.removeItem('access_token');
}

export function tokenGetter() {
    return localStorage.getItem('access_token');
}

export function tokenSetter(access_token: string) {
    return localStorage.setItem('access_token', access_token);
}

export function refreshTokenClear() {
    return localStorage.removeItem('refresh_token');
}

export function refreshTokenGetter() {
    return localStorage.getItem('refresh_token');
}

export function refreshTokenSetter(refresh_token: string) {
    return localStorage.setItem('refresh_token', refresh_token);
}

@Injectable({ providedIn: 'root' })
export class AuthorizationService {
    private static instances = 0;
    private static MAX_TIMEOUT_VALUE = 2147483647;
    constructor(private http: FitApiClientService, private jwtHelper: JwtHelperService, private router: Router, private fitConfig: FitConfigProviderService) {
        this.onServiceInit();
    }

    get user() {
        return this._user;
    }

    public async login(username: string, password: string) {
        const grant_type = "password";
        const client_id = this._fitConfig.client_id;
        const client_secret = this._fitConfig.client_secret;

        const { data, error } = await this.http.post<TokenResponse>('/oauth/token', { grant_type, client_id, client_secret, username, password });
        if (!error) {
            this.setToken(data.access_token);
            this.setRefreshToken(data.refresh_token);
            this.initRefreshTokenTimer();
            this.fetchUser();
        }

        return { data, error };
    }

    public async logout() {
        if (this.isAuthenticated()) {
            await this.http.logout();
        }
        this.clearToken()
        this._user = {} as any;
        this.router.navigateByUrl('/auth/login');
    }

    public requestOneTimePassword(destination: string) {
        return this.http.post<null>('/forgotten-password/send-otp', { destination });
    }

    public changePassword(request: { destination: string; code: string; password: string; }) {
        return this.http.post<null>('/forgotten-password/change-password', request);
    }

    public async refreshToken() {
        const refresh_token = refreshTokenGetter();
        if (!refresh_token) {
            return false;
        }
        const grant_type = "refresh_token";
        const client_id = this._fitConfig.client_id;
        const client_secret = this._fitConfig.client_secret;

        const { data, error } = await this.http.post<TokenResponse>('/oauth/token', { grant_type, client_id, client_secret, refresh_token });
        if (!error) {
            this.setToken(data.access_token);
            this.setRefreshToken(data.refresh_token);
            this.initRefreshTokenTimer();
            return true;
        } else {
            await this.logout();
            return false;
        }
    }

    public isAuthenticated(): boolean {
        return !this.jwtHelper.isTokenExpired();
    }

    public clearToken() {
        tokenClear();
        refreshTokenClear();
    }

    private setToken(access_token: string) {
        tokenSetter(access_token);
    }

    private setRefreshToken(refresh_token: string) {
        refreshTokenSetter(refresh_token);
    }

    private initRefreshTokenTimer() {
        if (!this.isAuthenticated()) {
            return;
        }
        const expiresAt = this.jwtHelper.getTokenExpirationDate();

        let runIn = expiresAt.getTime() - Date.now() - (180 * 1000);
        if (runIn < 1) {
            this.refreshToken();
            return;
        }

        if (runIn > AuthorizationService.MAX_TIMEOUT_VALUE) {
            runIn = AuthorizationService.MAX_TIMEOUT_VALUE;
        }

        this.refreshTokenTimer = setTimeout(() => {
            this.refreshToken();
        }, runIn);
    }

    private clearRefreshTokenTimer() {
        clearTimeout(this.refreshTokenTimer);
    }

    private async fetchUser() {
        const { data, error } = await this.http.get<User>('/user');
        if (!error) {
            this._user = data;
        }
    }

    private async onServiceInit() {
        AuthorizationService.instances++;
        const config = await this.fitConfig.getConfig();
        this._fitConfig = config
        const isAuthenticated = this.isAuthenticated()
        if (isAuthenticated) {
            this.initRefreshTokenTimer();
            this.fetchUser();
        } else {
            const success = await this.refreshToken();
            if (success) {
                this.router.navigateByUrl('/');
            }
        }
    }

    private refreshTokenTimer;

    private _user: User = {} as User;
    private _fitConfig: FitAppConfig
}
