import { TestBed } from '@angular/core/testing';

import { FitConfigProviderService } from './fit-config-provider.service';

describe('FitConfigProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FitConfigProviderService = TestBed.inject(FitConfigProviderService);
    expect(service).toBeTruthy();
  });
});
