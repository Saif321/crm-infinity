import { Component } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { LanguagesService, Language } from '../../languages.service';

@Component({
    selector: 'languages',
    templateUrl: './languages.component.html',
    styleUrls: ['./languages.component.scss'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: LanguagesComponent,
        multi: true
    }]
})
export class LanguagesComponent implements ControlValueAccessor {
    constructor(private languageService: LanguagesService) { this.loadLanguages(); }

    public languages: Language[] = [];

    public selectedLanguageId: number = 0;

    public touched = false;

    public isDisabled = false;

    public onValidatorChange: () => void = () => { };

    public onChange: (value: number) => void = (value: number) => { };

    public onTouched: () => void = () => { };

    public get isDefaultLanguage() {
        const selectedLanguage = this.languages.find(l => l.id === this.selectedLanguageId);
        if (!selectedLanguage) {
            return false;
        }
        return selectedLanguage.is_default;
    }

    public get defaultLanguage() { return this.languages.find(l => l.is_default ); }

    public writeValue(value: number): void {
        this.selectedLanguageId = value;
    }
    public registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    public registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }
    public setDisabledState?(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

    private async loadLanguages() {
        const { data } = await this.languageService.languages();
        this.languages = data;
        this.selectedLanguageId = data.find(l => l.is_default).id;
    }
}
