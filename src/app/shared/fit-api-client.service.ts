import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpEventType, HttpEvent } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { FitConfigProviderService } from './fit-config-provider.service';
import { NotificationsService } from './notifications.service';

export interface ApiResponse<T> {
    success: boolean;
    errorCode: number;
    errorMessage: string[];
    data: T;
}

export interface FitList<T> {
    current_page: number;
    first_page_url: string;
    from: number;
    last_page: number;
    last_page_url: string;
    next_page_url: string;
    path: string;
    per_page: number;
    prev_page_url: null
    to: number;
    total: number;
    data: T[];
}

interface UploadStatus<T> {
    progress?: number;
    response?: T;
    error?: string;
}

function isApiResponse<T>(object: any): object is ApiResponse<T> {
    return 'success' in object && 'errorCode' in object && 'errorMessage' in object;
}

function mapAsyncError<T>(promise: Promise<ApiResponse<T>>): Promise<{ data: T, error: ApiResponse<any> }> {
    return promise.then(data => {
        if((data as any).token_type){
            return {
                data: data as any as T,
                error: undefined
            };
        }

        if (!data.success) {
            throw data;
        }
        return {
            data: data.data,
            error: undefined
        };
    }).catch((error: HttpErrorResponse | ApiResponse<T>) => {
        if (!isApiResponse(error)) {
            if (isApiResponse(error.error)) {
                return {
                    data: undefined,
                    error: error.error
                };
            }
            return {
                data: undefined,
                error: {
                    success: false,
                    errorCode: error.status,
                    errorMessage: [error.message],
                    data: null
                }
            };
        } else {
            return {
                data: undefined,
                error: error
            };
        }
    });
}

@Injectable({providedIn: 'root'})
export class FitApiClientService {
    constructor(private http: HttpClient, private fitConfig: FitConfigProviderService, private notifications: NotificationsService) { }

    public async get<T>(path: string, params?: { [param: string]: string }) {
        const url = await this.createUrl(path);
        const { data, error } = await mapAsyncError<T>(this.http.get<ApiResponse<T>>(url, { params }).toPromise());
        if (error) {

            this.notifications.error('Error', error.errorMessage.join('\r\n'));
        }
        return { data, error };
    }

    public async post<T>(path: string, body) {
        const url = await this.createUrl(path);
        const { data, error } = await mapAsyncError<T>(this.http.post<ApiResponse<T>>(url, body).toPromise());
        if (error) {
            this.notifications.error('Error', error.errorMessage.join('\r\n'));
        }
        return { data, error };
    }

    public async delete<T>(path: string, params?: { [param: string]: string }) {
        const url = await this.createUrl(path);
        const { data, error } = await mapAsyncError<T>(this.http.delete<ApiResponse<T>>(url, { params }).toPromise());
        if (error) {
            this.notifications.error('Error', error.errorMessage.join('\r\n'));
        }
        return { data, error };
    }

    public async put<T>(path: string, body: any, params?: { [param: string]: string }) {
        const url = await this.createUrl(path);
        const { data, error } = await mapAsyncError<T>(this.http.put<ApiResponse<T>>(url, body, { params }).toPromise());
        if (error) {
            this.notifications.error('Error', error.errorMessage.join('\r\n'));
        }
        return { data, error };
    }

    public async logout() {
        const url = await this.createUrl('/logout');
        return this.http.post(url, {});
    }

    public async uploadFileToGallery<T>(form: FormData) {
        let uploadURL = await this.createUrl('/admin/gallery');
        return this.http.post<any>(uploadURL, form, {
            reportProgress: true,
            observe: 'events'
        }).pipe(
            map<HttpEvent<any>, UploadStatus<ApiResponse<T>>>((event) => {
                switch (event.type) {
                    case HttpEventType.UploadProgress:
                        const progress = Math.round(100 * event.loaded / event.total);
                        return { progress: progress };
                    case HttpEventType.Response:
                        const body: ApiResponse<T> = event.body;
                        if (body.errorMessage && body.errorMessage.length > 0) {
                            this.notifications.error('Error', body.errorMessage.join('\r\n'));
                        }
                        return { response: event.body };
                    default:
                        const err = `Unhandled event: ${event.type}`;
                        return { error: err };
                }
            })
        );
    }

    private async createUrl(path: string) {
        const config = await this.fitConfig.getConfig();
        return config.apiUrl + path;
    }
}
