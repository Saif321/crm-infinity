import { OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { FitList } from './fit-api-client.service';

export abstract class PageableComponent<T> implements OnInit {
    get list() { return this._list ? this._list.data : []; }

    get page() { return this._page; }

    get totalItems() { return this._list ? this._list.total : 0; }

    get itemsPerPage() { return this._list ? this._list.per_page : 0; }

    get hasPagination() { return this._list && this._list.last_page != 1; }

    public searchQuery = '';

    public searchQueryStream = new Subject<string>();

    public ngOnInit() {
        this.searchQueryStream.pipe(
            debounceTime(500)
        ).subscribe(() => (
            this._page = 1,
            this.load()));
    }

    public onPageChanged($event: { page: number, itemsPerPage: number }) {
        this._page = $event.page;
        this.load();
    }

    public clearSearch() {
        this.searchQuery = '';
        this.load();
    }

    public abstract async load();

    protected _list: FitList<T>;
    protected _page = 1;
}
