import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { ToastrModule } from 'ngx-toastr';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { FitConfigProviderService } from './fit-config-provider.service';
import { UnauthorizedRequestInterceptor } from './unauthorized-request.interceptor';
import { AuthorizationNeededGuard } from './guards/authorization-needed.guard';
import { NotificationsService } from './notifications.service';
import { LanguagesService } from './languages.service';
import { LanguagesComponent } from './components/languages/languages.component';
import { DateFormatPipe, DATE_FORMAT_PIPE_FORMATS, DateFormatSettings } from './pipes/date-format.pipe';
import { CounterComponent } from './components/counter/counter.component';
import { CloudMessagingService } from './shared.service';

const dateFormatsFactory = (config: FitConfigProviderService): DateFormatSettings => {
    return {
        dateFormat: config.dateFormat,
        dateTimeFormat: config.dateTimeFormat,
        timeFormat: config.timeFormat
    };
};

@NgModule({
    declarations: [LanguagesComponent, DateFormatPipe, CounterComponent],
    imports: [
        CommonModule,
        FormsModule,
        ToastrModule.forRoot({
            iconClasses: {
                error: 'toast-custom-error',
                info: 'toast-custom-info',
                success: 'toast-custom-success',
                warning: 'toast-custom-warning'
            }
        }),
        ButtonsModule.forRoot()
    ],
    exports: [LanguagesComponent, DateFormatPipe, CounterComponent],
    providers: [
        AuthorizationNeededGuard,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: UnauthorizedRequestInterceptor,
            multi: true
        },
        NotificationsService,
        CloudMessagingService,
        LanguagesService,
        { provide: DATE_FORMAT_PIPE_FORMATS, useFactory: dateFormatsFactory, deps: [FitConfigProviderService] },
    ]
})
export class SharedModule { }
