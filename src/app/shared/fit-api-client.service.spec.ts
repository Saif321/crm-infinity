import { TestBed } from '@angular/core/testing';

import { FitApiClientService } from './fit-api-client.service';

describe('FitApiClientService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FitApiClientService = TestBed.inject(FitApiClientService);
    expect(service).toBeTruthy();
  });
});
