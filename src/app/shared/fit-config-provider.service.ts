import { Injectable } from '@angular/core';

import { FitAppConfig } from './fit-app-config';

import { environment } from '@environments/environment';

function load() {
    const jsonFile = `assets/config/config.${environment.name}.json`;
    new Promise<void>((resolve, reject) => {
        fetch(jsonFile)
            .then(res => res.json())
            .then((response: FitAppConfig) => {
                FitConfigProviderService.settings = <FitAppConfig>response;
                resolve();
            }).catch((response: any) => {
                reject(`Could not load file '${jsonFile}': ${JSON.stringify(response)}`);
            });
    });
}

@Injectable({ providedIn: 'root' })
export class FitConfigProviderService {
    public static settings: FitAppConfig;

    get apiUrl() { return FitConfigProviderService.settings.apiUrl; }
    get dateFormat() { return FitConfigProviderService.settings.format.date; }
    get timeFormat() { return FitConfigProviderService.settings.format.time; }
    get dateTimeFormat() { return FitConfigProviderService.settings.format.dateTime; }

    public async getConfig() {
        const jsonFile = `assets/config/config.${environment.name}.json`;
        return new Promise<FitAppConfig>((resolve, reject) => {
            if (FitConfigProviderService.settings) {
                resolve(FitConfigProviderService.settings);
            } else {
                fetch(jsonFile)
                    .then(res => res.json())
                    .then((response: FitAppConfig) => {
                        FitConfigProviderService.settings = <FitAppConfig>response;
                        resolve(response);
                    }).catch((response: any) => {
                        reject(`Could not load file '${jsonFile}': ${JSON.stringify(response)}`);
                    });
            }
        });
    }
}

load();
