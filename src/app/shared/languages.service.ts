import { Injectable } from '@angular/core';
import { FitApiClientService } from './fit-api-client.service';

export interface Language {
    id: number;
    language: string;
    is_default: boolean;
    iso: string;
    created_at: string;
    updated_at: string;
}

@Injectable()
export class LanguagesService {
    constructor(private api: FitApiClientService) { }

    public languages() {
        return this.api.get<Language[]>('/language');
    }
}