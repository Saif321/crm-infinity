export interface FitAppConfig {
    env: {
        name: string;
    };
    apiUrl: string;
    format: {
        date: string;
        time: string;
        dateTime: string;
    };
    grant_type_login: string;
    grant_type_refresh: string;
    client_id: number;
    client_secret: string;
}
