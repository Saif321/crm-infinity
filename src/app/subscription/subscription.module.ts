import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';

import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgSelectModule } from '@ng-select/ng-select';

import { SharedModule } from '@shared/shared.module';
import { InteractionModule } from '@app/interaction/interaction.module';

import { SubscriptionRoutingModule } from './subscription-routing.module';
import { ListComponent } from './list/list.component';
import { SubscriptionService } from './subscription.service';


@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    SubscriptionRoutingModule,
    ModalModule.forRoot(),
    FormsModule,
    PaginationModule.forRoot(),
    NgSelectModule,
    SharedModule,
    InteractionModule
  ],
  providers: [SubscriptionService]
})
export class SubscriptionModule { }
