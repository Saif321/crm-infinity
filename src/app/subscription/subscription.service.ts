import { Injectable } from '@angular/core';

import { FitApiClientService, FitList } from '@shared/fit-api-client.service';

interface SubscriptionBase {
    user_id: string;
    email: string;
    package_name: string;
    valid_until: string;
    last_renewal_attempt: string;
    renew_attempts: number;
    renew_stopped: number;
    payment_method: string;
    charged_value: string;
    currency_code: string;
    is_free_trial: number;
}

export { FitList, SubscriptionBase }

@Injectable()
export class SubscriptionService {
    constructor(
        private fitApi: FitApiClientService) { }

    public search(query: string, page: number, onlyActive: boolean, includeFreeTrial: boolean) {
        let params: any = {
            search: query,
            page: page.toString()
        }

        if (typeof onlyActive === 'boolean') {
            onlyActive ? params.only_active = '1' : params.only_active = '0';
        }

        if (typeof includeFreeTrial === 'boolean') {
            includeFreeTrial ? params.include_free_trial = '1' : params.include_free_trial = '0';
        }

        return this.fitApi.get<FitList<SubscriptionBase>>('/admin/reports/user-packages', params);
    }

}
