import { Component, OnInit } from '@angular/core';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';

import { SubscriptionService, SubscriptionBase } from '../subscription.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent extends PageableComponent<SubscriptionBase> {
    constructor(
        protected subscriptionService: SubscriptionService,
        private interaction: InteractionService
    ) {
        super();
        this.load();
    }

    public onlyActive = true;
    public includeFreeTrial = true;


    public async load() {
        const { data } = await this.subscriptionService.search(
            this.searchQuery,
            this._page,
            this.onlyActive,
            this.includeFreeTrial
        );
        this._list = data;
    }
}
