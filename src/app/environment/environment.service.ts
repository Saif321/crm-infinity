import { Injectable } from '@angular/core';

import { FitApiClientService, FitList } from '@shared/fit-api-client.service';

interface EnvironmentBase {
    id?: number;
    name: string;
    description: string;
    order: number;
    updated_at?: string;
    created_at?: string;
}
interface EnvironmentTranslation {
    language_id: number;
    description: string;
    name: string;
}
interface Environment extends EnvironmentBase {
    translations: EnvironmentTranslation[];
}

export { FitList, EnvironmentBase, Environment, EnvironmentTranslation };

@Injectable({ providedIn: 'root' })
export class EnvironmentService {
    constructor(private fitApi: FitApiClientService) { }

    public search(query: string, page: number) {
        return this.fitApi.get<FitList<EnvironmentBase>>('/admin/exercise-environment', {
            search: query,
            page: page.toString()
        });
    }

    public get(id: number) {
        return this.fitApi.get<Environment>('/admin/exercise-environment/' + id);
    }

    public create(environment: Environment) {
        return this.fitApi.put<Environment>('/admin/exercise-environment/0', this.mapToRequest(environment));
    }

    public update(environment: Environment) {
        return this, this.fitApi.put<Environment>('/admin/exercise-environment/' + environment.id, this.mapToRequest(environment));
    }

    public delete(id: number) {
        return this.fitApi.delete<any>('/admin/exercise-environment/' + id);
    }

    private mapToRequest(environment: Environment): Environment {
        return {
            name: environment.name,
            order: environment.order,
            description: environment.description,
            translations: environment.translations.filter(t => t.name)
                .map(t => {
                    return {
                        language_id: t.language_id,
                        name: t.name,
                        description: t.description
                    }
                })
        };
    }
}
