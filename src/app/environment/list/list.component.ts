import { Component } from '@angular/core';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';

import { EnvironmentService, EnvironmentBase } from '../environment.service';

@Component({
    selector: 'environment-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent extends PageableComponent<EnvironmentBase>  {

    constructor(
        protected environmentService: EnvironmentService,
        private interaction: InteractionService
    ) {
        super();
        this.load();
    }

    public async load() {
        const { data } = await this.environmentService.search(
            this.searchQuery,
            this._page
        );
        this._list = data;
    }

    public async delete(id: number) {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to delete environment?'
        )
        if (isConfirmed) {
            await this.environmentService.delete(id);
            this.load();
        }
    }
}
