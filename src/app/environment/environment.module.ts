import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PaginationModule } from 'ngx-bootstrap/pagination';

import { SharedModule } from '@shared/shared.module';
import { InteractionModule } from '@app/interaction/interaction.module';

import { EnvironmentRoutingModule } from './environment-routing.module';
import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';

@NgModule({
  declarations: [ListComponent, FormComponent],
  imports: [
    CommonModule,
    FormsModule,
    EnvironmentRoutingModule,
    SharedModule,
    InteractionModule,
    PaginationModule.forRoot()
  ],
  providers: []
})
export class EnvironmentModule { }
