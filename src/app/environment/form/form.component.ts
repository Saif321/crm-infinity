import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';

import { EnvironmentService, Environment, EnvironmentTranslation } from '../environment.service';
import { LanguagesComponent } from '@shared/components/languages/languages.component';
import { NotificationsService } from '@shared/notifications.service';

@Component({
    selector: 'environment-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

    constructor(
        private environments: EnvironmentService,
        private route: ActivatedRoute,
        private router: Router,
        private notifications: NotificationsService
    ) { }

    public selectedLanguageId = 1;
    public isDisabled = false;

    @ViewChild(LanguagesComponent, { static: true })
    languagesComponent: LanguagesComponent;

    public isEditMode = false;

    public get isDefaultLanguage() {
        return this.languagesComponent.isDefaultLanguage;
    }

    public get environment() {
        return this._environment;
    }

    public get environmentTranslation(): EnvironmentTranslation {
        const environment = this._environment;
        if (!environment) {
            return;
        }
        let translation = environment.translations.find(
            t => t.language_id === this.selectedLanguageId
        );
        if (!translation) {
            translation = {
                name: environment.name,
                description: environment.description,
                language_id: this.selectedLanguageId
            };
            environment.translations.push(translation);
        }
        return translation;
    }
    public onEnvironmentSubmit() {
        if (this.isEditMode) {
            this.update();
        } else {
            this.create();
        }
    }

    public async ngOnInit() {
        this.route.paramMap.subscribe(params => {
            if (!params.has('id')) {
                this.isEditMode = false;
                return;
            }
            this.isEditMode = true;
            this.environments.get(parseInt(params.get('id'))).then(res => {
                this._environment = res.data;
            });
        });
    }

    public async create() {
        this.isDisabled = true;
        const defaultTranslation = this.getDefaultTranslation();
        this.environment.name = defaultTranslation.name;
        this.environment.description = defaultTranslation.description;
        const { data, error } = await this.environments.create(this.environment);
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'Create was successful!');
            this.router.navigate(['/environment/edit', data.id]);
        }
    }

    public async update() {
        this.isDisabled = false;
        const defaultTranslation = this.getDefaultTranslation();
        this.environment.name = defaultTranslation.name;
        this.environment.description = defaultTranslation.description;
        const { data, error } = await this.environments.update(this.environment);
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'The update was successful!');
        }
    }

    private getDefaultTranslation() {
        return this._environment.translations.find(t => t.language_id == this.languagesComponent.defaultLanguage.id);
    }

    private _environment: Environment = {
        name: '',
        order: 0,
        description: '',
        translations: []
    };
}
