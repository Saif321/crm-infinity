import { Injectable } from '@angular/core';

import { LevelService, LevelBase } from '@app/level/level.service';
import { EnvironmentService, EnvironmentBase } from '@app/environment/environment.service';
import { GoalService, GoalBase } from '@app/goal/goal.service';
import { UserListItem } from '@app/user/user.service';
import { FitApiClientService, FitList } from '@shared/fit-api-client.service';

import { Program, ProgramTranslation, ProgramItemListResponse, ProgramResponse } from './types';

export { LevelBase, EnvironmentBase, GoalBase, UserListItem }

export interface WeekItemRequest {
    is_locked: boolean;
    order: number;
    training_id: number;
    is_rest_day: boolean;
}

export interface ProgramWeekRequest {
    order: number;
    items: WeekItemRequest[];
}

export interface ProgramTranslationRequest {
    name: string;
    description: string;
    language_id: number;
    image_id: number;
    video_id: number;
}

export interface ProgramRequest {
    id?: number;
    name: string;
    description: string;
    workout_days_per_week: number;
    program_length_in_weeks: number;
    level_ids: number[];
    exercise_environment_ids: number[];
    goal_ids: number[];
    is_published: boolean;
    is_free: boolean;
    gender?: 'male' | 'female';
    video_id: number;
    image_id: number;
    weeks: ProgramWeekRequest[];
    translations: ProgramTranslationRequest[];
}

@Injectable()
export class ProgramService {
    constructor(
        private fitApi: FitApiClientService,
        private levelService: LevelService,
        private environmentService: EnvironmentService,
        private goalService: GoalService) { }

    public search(query: string, page: number) {
        var params: any = {
            search: query,
            page: page.toString()
        }
        return this.fitApi.get<FitList<ProgramItemListResponse>>('/admin/program', params);
    }

    public get(id: number) {
        return this.fitApi.get<ProgramResponse>('/admin/program/' + id);
    }

    public getProgramUsers(programId: number) {
        return this.fitApi.get<UserListItem[]>(`/admin/program/${programId}/user`);
    }

    public create(request: ProgramRequest) {
        return this.fitApi.put<ProgramItemListResponse>('/admin/program/0', request);
    }

    public update(request: ProgramRequest) {
        return this.fitApi.put<ProgramRequest>('/admin/program/' + request.id, request);
    }

    public delete(id: number) {
        return this.fitApi.delete<ProgramItemListResponse>('/admin/program/' + id);
    }

    public searchLevels(query: string, page: number) {
        return this.levelService.search(query, page);
    }

    public searchEnvironments(query: string, page: number) {
        return this.environmentService.search(query, page);
    }

    public searchGoals(query: string, page: number) {
        return this.goalService.search(query, page);
    }

    public publishProgram(programId: number) {
        return this.fitApi.put<any>(`/admin/program/${programId}/publish`, null);
    }

    public assignProgramToUser(programId: number, userId: number) {
        return this.fitApi.put<any>(`/admin/program/${programId}/user/${userId}`, undefined);
    }

    public mapToProgramRequest(program: Program, programTranslation: ProgramTranslation): ProgramRequest {
        return {
            id: program.id,
            name: programTranslation.name,
            description: programTranslation.description,
            exercise_environment_ids: program.exercise_environment_ids,
            level_ids: program.level_ids,
            goal_ids: program.goal_ids,
            image_id: programTranslation.image.id,
            video_id: programTranslation.video ? programTranslation.video.id : null,
            is_free: program.is_free,
            gender: program.gender,
            is_published: program.is_published,
            translations: program.translations.filter(t => !!t.name)
                .map((t): ProgramTranslationRequest => {
                    return {
                        name: t.name,
                        description: t.description,
                        image_id: t.image.id,
                        video_id: t.video ? t.video.id : null,
                        language_id: t.language_id
                    };
                }),
            weeks: program.weeks.map((w, i) => {
                return {
                    order: i + 1,
                    items: w.items.map((item, index) => {
                        return {
                            is_locked: item.is_locked,
                            order: index + 1,
                            training_id: item.training_id ? item.training_id : null,
                            is_rest_day: item.is_rest_day
                        }
                    })
                }
            }),
            workout_days_per_week: program.workout_days_per_week,
            program_length_in_weeks: program.weeks.length
        };
    }
}
