interface ProgramStatus {
    name: string;
    value: boolean;
}

const statuses: ProgramStatus[] = [
    { value: false, name: 'Draft' },
    { value: true, name: 'Published' }
];

export { ProgramStatus, statuses };