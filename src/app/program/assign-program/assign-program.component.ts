import { Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { distinctUntilChanged, debounceTime } from 'rxjs/operators';

import { PopoverDirective } from 'ngx-bootstrap/popover';
import { NotificationsService } from '@shared/notifications.service';

import { UserService } from '@app/user/user.service';
import { Program } from '../types';

import { ProgramService, UserListItem } from '../program.service';

@Component({
    selector: 'assign-program',
    templateUrl: './assign-program.component.html',
    styleUrls: ['./assign-program.component.scss']
})
export class AssignProgramComponent {
    constructor(
        protected programService: ProgramService,
        protected userService: UserService,
        private notification: NotificationsService
    ) {
        this.initUserSearch();
    }

    @Input() public program: Program;
    @Output() public popoverOpen = new EventEmitter<AssignProgramComponent>(true);

    @ViewChild(PopoverDirective)
    public pop: PopoverDirective;

    public usersTypeAhead = new EventEmitter<string>();
    public users: UserListItem[] = [];
    public selectedUser: UserListItem;

    public async assignProgramToUser(program: Program, selectedUser: UserListItem, pop: PopoverDirective) {
        const { data, error } = await this.programService.assignProgramToUser(program.id, selectedUser.id);
        if (!error) {
            this.notification
                .success('Success', program.name + ' assigned successfuly to ' + selectedUser.first_name + ' ' + selectedUser.last_name);
            pop.hide();
        }
    }

    public close() {
        this.pop.hide();
    }

    public onOpen() {
        this.popoverOpen.emit(this);
    }

    private initUserSearch() {
        this.usersTypeAhead.pipe(
            distinctUntilChanged(),
            debounceTime(400)
        ).subscribe(term => {
            this.userService.searchUser(term, 1)
                .then(({ data, error }) => {
                    this.users = [];
                    if (!error) {
                        this.users = data.data;
                    }
                });
        });
    }

}
