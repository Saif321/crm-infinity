import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignProgramComponent } from './assign-program.component';

describe('AssignProgramComponent', () => {
  let component: AssignProgramComponent;
  let fixture: ComponentFixture<AssignProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
