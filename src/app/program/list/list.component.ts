import { Component, ViewChildren, QueryList } from '@angular/core';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';
import { UserService } from '@app/user/user.service';
import { CloudMessagingService } from '@shared/shared.service';

import { ProgramService, UserListItem } from '../program.service';
import { Program } from '../types';
import { AssignProgramComponent } from '../assign-program/assign-program.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent extends PageableComponent<any> {
    constructor(
        protected programService: ProgramService,
        protected userService: UserService,
        protected cloudMessagingService: CloudMessagingService,
        private interaction: InteractionService
    ) {
        super();
        this.load();
    }

    @ViewChildren(AssignProgramComponent)
    public assignProgramButtons: QueryList<AssignProgramComponent>;

    public showFilters = false;
    public programUsers: UserListItem[] = []
    public loadingProgramUsers = false;

    public onAssignPopoverOpen(cmp: AssignProgramComponent) {
        this.assignProgramButtons.forEach(button => {
            if (button !== cmp) {
                button.close();
            }
        })
    }

    public async load() {
        const { data } = await this.programService.search(
            this.searchQuery,
            this._page
        );
        this._list = data;
    }

    public async delete(id: number) {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to delete this program?'
        );
        if (isConfirmed) {
            await this.programService.delete(id);
            this.load();
        }
    }

    public async publish(program: Program) {
        let isConfirmed = await this.interaction.confirm(program.is_published ? 'Are you sure you want to unpublish program?' : 'Are you sure you want to publish program?');
        if (isConfirmed) {
            await this.programService.publishProgram(program.id);
            this.load();
        }
    }

    public async loadWorkoutProgramUsers(program: Program) {
        this.programUsers = [];
        this.loadingProgramUsers = true;
        const { data, error } = await this.programService.getProgramUsers(program.id);
        this.loadingProgramUsers = false;
        if (!error) {
            this.programUsers = data;
        }
    }
    public async notification() {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to push this notification?'
        );
        if (isConfirmed) {
        }
    }
    public async notify(id: number) {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to push this notification?'
        );
        if (isConfirmed) {
            await this.cloudMessagingService.notify({ object_id: id, object_type: 'Program' });
        }
    }
}
