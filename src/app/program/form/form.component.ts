import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { LanguagesComponent } from '@shared/components/languages/languages.component';
import { NotificationsService } from '@shared/notifications.service';

import { ProgramService, LevelBase, EnvironmentBase, GoalBase } from '../program.service';
import { ProgramStatus, statuses } from '../program-status';
import { Program, ProgramWeek } from '../types';

interface Gender {
    key: string;
    label: string;
}

const male = { key: 'male', label: 'Male' };
const female = { key: 'female', label: 'Female' };

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
    constructor(
        private programService: ProgramService,
        private route: ActivatedRoute,
        private router: Router,
        private notifications: NotificationsService) {
        this.loadLevels();
        this.loadEnvironments();
        this.loadGoals();
    }

    public selectedLanguageId = 1;
    public isDisabled = false;

    @ViewChild(LanguagesComponent, { static: true })
    public languagesComponent: LanguagesComponent;

    public isEditMode = false;

    public levels: LevelBase[] = [];
    public environments: EnvironmentBase[] = [];
    public goals: GoalBase[] = [];
    public statuses: ProgramStatus[] = statuses;

    public genderOptions: Gender[] = [male, female];

    public get isDefaultLanguage() {
        return this.languagesComponent.isDefaultLanguage;
    }

    public get program() { return this._program; }

    public get programTranslation() {
        const program = this._program;
        if (!program) {
            return;
        }
        let translation = program.translations.find(
            t => t.language_id === this.selectedLanguageId
        );
        if (!translation) {
            translation = {
                name: program.name,
                description: program.description,
                language_id: this.selectedLanguageId,
                image: undefined,
                video: undefined,
                image_id: undefined,
                video_id: undefined
            };
            program.translations.push(translation);
        }
        return translation;
    }

    public ngOnInit() {
        this.route.paramMap.subscribe(params => {
            if (!params.has('id')) {
                this.isEditMode = false;
                return;
            }
            this.isEditMode = true;
            this.programService.get(parseInt(params.get('id'))).then(res => {
                const p = res.data;
                this._program = {
                    id: p.id,
                    name: p.name,
                    description: p.description,
                    goal_ids: p.goals.map(g => g.id),
                    exercise_environment_ids: p.exercise_environments.map(e => e.id),
                    workout_days_per_week: p.workout_days_per_week,
                    is_free: p.is_free,
                    is_published: p.is_published,
                    image_id: p.image_id,
                    level_ids: p.levels.map(l => l.id),
                    video_id: p.video_id,
                    gender: p.gender,
                    translations: p.translations,
                    weeks: p.weeks.sort((w1, w2) => w1.order > w2.order ? 1 : -1).map(w => {
                        return {
                            id: w.id,
                            items: w.items.map(i => {
                                return {
                                    is_locked: i.is_locked,
                                    training_id: i.training_id,
                                    name: i.training ? i.training.name : '',
                                    is_rest_day: i.is_rest_day
                                };
                            })
                        }
                    })
                };
            });
        });
    }

    public addWeek() {
        this._program.weeks.push({
            id: undefined,
            items: []
        });
    }

    public onWeekDeleteClick($event: ProgramWeek) {
        this._program.weeks.splice(this._program.weeks.indexOf($event), 1);
    }

    public onWeekUpdated() {
        this.onProgramSubmit();
    }

    public onProgramSubmit() {
        if (this.isEditMode) {
            this.update();
        } else {
            this.create();
        }
    }

    private async create() {
        this.isDisabled = true;
        const request = this.programService.mapToProgramRequest(this._program, this.program.translations.find(t => t.language_id == this.languagesComponent.defaultLanguage.id));
        const { data, error } = await this.programService.create(request);
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'Create was successful!');
            this.router.navigate(['/program/edit', data.id]);
        }
    }

    private async update() {
        this.isDisabled = true;
        const request = this.programService.mapToProgramRequest(this._program, this.program.translations.find(t => t.language_id == this.languagesComponent.defaultLanguage.id));
        const { data, error } = await this.programService.update(request);
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'The update was successful!');
        }
    }

    private async loadLevels() {
        const { data, error } = await this.programService.searchLevels('', 1);
        if (!error) {
            this.levels = data.data;
        }
    }

    private async loadEnvironments() {
        const { data, error } = await this.programService.searchEnvironments('', 1);
        if (!error) {
            this.environments = data.data;
        }
    }

    private async loadGoals() {
        const { data, error } = await this.programService.searchGoals('', 1);
        if (!error) {
            this.goals = data.data;
        }
    }

    public onWeekDrop(event: CdkDragDrop<string[]>) {
        moveItemInArray(this._program.weeks, event.previousIndex, event.currentIndex);
    }

    private _program: Program = {
        name: '',
        description: '',
        exercise_environment_ids: [],
        goal_ids: [],
        image_id: undefined,
        is_free: false,
        gender: undefined,
        is_published: false,
        level_ids: [],
        video_id: undefined,
        translations: [],
        weeks: [],
        workout_days_per_week: 0
    };
}
