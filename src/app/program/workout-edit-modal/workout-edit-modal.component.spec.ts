import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkoutEditModalComponent } from './workout-edit-modal.component';

describe('WorkoutEditModalComponent', () => {
  let component: WorkoutEditModalComponent;
  let fixture: ComponentFixture<WorkoutEditModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkoutEditModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkoutEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
