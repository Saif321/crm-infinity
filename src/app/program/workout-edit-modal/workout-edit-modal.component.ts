import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

import { FormComponent as WorkoutFormComponent } from '@app/workout/form/form.component';

import { WeekItem } from '../types';
import { Workout } from '@app/workout/types';

@Component({
    selector: 'app-workout-edit-modal',
    templateUrl: './workout-edit-modal.component.html',
    styleUrls: ['./workout-edit-modal.component.scss']
})
export class WorkoutEditModalComponent implements AfterViewInit {
    constructor(public bsModalRef: BsModalRef) { }

    @ViewChild(WorkoutFormComponent)
    public form: WorkoutFormComponent;

    public weekItem: WeekItem;

    public onCloseClick() {
        this.onClose(this.weekItem);
        this.bsModalRef.hide();
    }

    public onUpdateFinished(workout: Workout) {
        this.weekItem.training_id = workout.id;
        this.weekItem.name = workout.name;
        this.onClose(this.weekItem);
        this.bsModalRef.hide();
    }

    public ngAfterViewInit(): void {
        this.form.startEditing(this.weekItem.training_id, true);
    }

    private onClose(item: WeekItem) { }
}
