import { GalleryItem } from '@app/gallery/gallery.service';
import { GoalBase } from '@app/goal/goal.service';
import { TrainingResponse } from '@app/workout/workout.service';
import { LevelBase } from '@app/level/level.service';
import { EnvironmentBase } from '@app/exercise/exercise.service';

export interface ProgramItemListResponse {
    id: number;
    level_id: number;
    name: string;
    description: string;
    workout_days_per_week: number;
    program_length_in_weeks: number;
    video_id: number;
    image_id: number;
    comment_count: number;
    like_count: number;
    review_count: number;
    avg_rating: number;
    is_published: boolean;
    is_free: boolean;
    created_at: string;
    updated_at: string;
    activated_count: number;
    deactivated_count: number;
    exercise_environment_id: number;
}

export interface ProgramWeekItemResponse {
    created_at: string;
    id: number;
    is_locked: boolean;
    is_rest_day: boolean;
    order: number;
    program_week_id: number;
    training: TrainingResponse;
    training_id: number;
    updated_at: string;
}

export interface ProgramWeekResponse {
    created_at: string;
    id: number;
    items: ProgramWeekItemResponse[]
    order: number;
    program_id: number;
    updated_at: string;
}

export interface ProgramTranslationResponse {
    language_id: number;
    name: string;
    description: string;
    image_id: number;
    video_id: number;
    image: GalleryItem;
    video: GalleryItem;
}

export interface ProgramResponse {
    avg_rating: number;
    comment_count: number;
    created_at: string;
    description: string;
    exercise_environment_id: number;
    id: number;
    image_id: number;
    is_free: boolean;
    is_published: boolean;
    levels: LevelBase[];
    like_count: number;
    name: string;
    program_length_in_weeks: number;
    gender?: 'male' | 'female';
    review_count: number;
    updated_at: string;
    video_id: number;
    workout_days_per_week: number;
    goals: GoalBase[];
    exercise_environments: EnvironmentBase[];
    translations: ProgramTranslationResponse[]
    weeks: ProgramWeekResponse[];
    image: GalleryItem;
    video: GalleryItem;
}

export interface WeekItem {
    is_locked: boolean;
    training_id?: number;
    name: string;
    is_rest_day: boolean;
}

export interface ProgramWeek {
    id?: number;
    items: WeekItem[];
}

export interface ProgramTranslation {
    name: string;
    description: string;
    language_id: number;
    video_id: number;
    image_id: number;
    image: GalleryItem;
    video: GalleryItem;
}

export interface Program {
    id?: number;
    name: string;
    description: string;
    workout_days_per_week: number;
    level_ids: number[];
    goal_ids: number[];
    exercise_environment_ids: number[];
    is_published: boolean;
    is_free: boolean;
    gender?: 'male' | 'female';
    video_id: number;
    image_id: number;
    weeks: ProgramWeek[];
    translations: ProgramTranslation[];
}