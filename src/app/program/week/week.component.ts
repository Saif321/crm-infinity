import { Component, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { BsModalService } from 'ngx-bootstrap/modal';

import { WorkoutService } from '@app/workout/workout.service';
import { NotificationsService } from '@shared/notifications.service';

import { ProgramWeek, WeekItem } from '../types';
import { WorkoutEditModalComponent } from '../workout-edit-modal/workout-edit-modal.component';

@Component({
    selector: 'week',
    templateUrl: './week.component.html',
    styleUrls: ['./week.component.scss']
})
export class WeekComponent {
    constructor(
        private workoutService: WorkoutService,
        private notifications: NotificationsService,
        private modalService: BsModalService,
        private cd: ChangeDetectorRef) { }

    @Input() index: number;
    @Input() public isDefaultLanguage = false;
    @Input()
    public get week() { return this._week; }
    public set week(week) {
        this._week = week;
        if (!week.id) {
            this.isCollapsed = false;
        }
    }

    @Output() deleteClick = new EventEmitter<ProgramWeek>(true);
    @Output() updated = new EventEmitter<void>(true);

    public isCollapsed = true;

    public async onAddWorkoutClick() {
        const workout = await this.workoutService.selectFromWorkout();
        if (!workout) {
            return;
        }
        this.pushWeekItem({
            is_locked: true,
            training_id: workout.id,
            name: workout.name,
            is_rest_day: false
        });
    }

    public async onAddRestDayClick(item: WeekItem) {
        this.pushWeekItem({
            is_locked: true,
            name: 'Rest day',
            is_rest_day: true
        }, item);
    }

    public onRoundItemDrop(event: CdkDragDrop<string[]>) {
        moveItemInArray(this.week.items, event.previousIndex, event.currentIndex);
    }

    public onDeleteClick() {
        this.deleteClick.emit(this.week);
    }

    public onItemDeleteClick(item: WeekItem) {
        this.week.items.splice(this.week.items.indexOf(item), 1);
    }

    public onEditWeekItemClick(item: WeekItem) {
        const promise = new Promise<WeekItem>((resolve, reject) => {
            const initialState = {
                onClose: (item: WeekItem) => {
                    resolve(item);
                },
                weekItem: item
            };
            this.modalService.show(WorkoutEditModalComponent, { initialState, backdrop: false, ignoreBackdropClick: true, class: 'workout-edit-modal' });
        });
        promise.then(item => {
            this.cd.markForCheck();
            this.updated.emit();
        });

        return promise;
    }

    private pushWeekItem(item: WeekItem, afterItem?: WeekItem) {
        if (this.week.items.length < 7) {
            if (afterItem) {
                this.week.items.splice(this.week.items.indexOf(afterItem) + 1, 0, item);
            } else {
                this.week.items.push(item);
            }
        } else {
            this.notifications.warning('Maximum reached', 'Week can have maximum 7 items.');
        }
    }

    private _week: ProgramWeek;
}
