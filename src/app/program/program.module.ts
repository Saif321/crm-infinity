import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgSelectModule } from '@ng-select/ng-select';

import { GalleryModule } from '@app/gallery/gallery.module';
import { WorkoutModule } from '@app/workout/workout.module';

import { SharedModule } from '@shared/shared.module';

import { ProgramRoutingModule } from './program-routing.module';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { ProgramService } from './program.service';
import { WeekComponent } from './week/week.component';
import { UserModule } from '@app/user/user.module';
import { AssignProgramComponent } from './assign-program/assign-program.component';
import { WorkoutEditModalComponent } from './workout-edit-modal/workout-edit-modal.component';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { PaginationModule } from 'ngx-bootstrap/pagination';

@NgModule({
    declarations: [FormComponent, ListComponent, WeekComponent, AssignProgramComponent, WorkoutEditModalComponent],
    imports: [
        CommonModule,
        FormsModule,
        ProgramRoutingModule,
        SharedModule,
        NgSelectModule,
        GalleryModule,
        DragDropModule,
        WorkoutModule,
        UserModule,
        WorkoutModule,
        PopoverModule.forRoot(),
        PaginationModule.forRoot()
    ],
    providers: [ProgramService]
})
export class ProgramModule { }
