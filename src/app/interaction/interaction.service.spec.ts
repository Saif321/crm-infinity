import { TestBed } from '@angular/core/testing';

import { InteractionService } from './interaction.service';

describe('InteractionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InteractionService = TestBed.inject(InteractionService);
    expect(service).toBeTruthy();
  });
});
