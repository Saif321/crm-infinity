import { Injectable } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { ConfirmComponent, ConfirmStatus } from './confirm/confirm.component';

@Injectable()
export class InteractionService {
    constructor(private modalService: BsModalService) { }

    public confirm(question: string): Promise<boolean> {
        const promise = new Promise<boolean>((resolve, reject) => {
            const initialState = {
                title: question,
                onClose: (status: ConfirmStatus) => {
                    if (status === ConfirmStatus.CONFIRMED) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                }
            };
            this.bsModalRef = this.modalService.show(ConfirmComponent, { initialState, backdrop: false, ignoreBackdropClick: true });
        })
        return promise;
    }

    private bsModalRef: BsModalRef;
}
