import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

export enum ConfirmStatus {
    CANCELED,
    CONFIRMED
}

@Component({
    selector: 'app-confirm',
    templateUrl: './confirm.component.html',
    styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent {
    constructor(public bsModalRef: BsModalRef) { }
    public title: string;
    public cancelBtnName: string = 'Cancel';
    public confirmBtnName: string = 'Confirm';

    public status: ConfirmStatus;

    public confirm() {
        this.status = ConfirmStatus.CONFIRMED;
        this.onClose(this.status);
        this.bsModalRef.hide();
    }

    public cancel() {
        this.status = ConfirmStatus.CANCELED;
        this.onClose(this.status);
        this.bsModalRef.hide();
    }

    private onClose(status: ConfirmStatus) { }
}
