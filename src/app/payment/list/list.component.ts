import { Component, TemplateRef } from '@angular/core';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';

import { PaymentService, PaymentBase } from '../payment.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends PageableComponent<PaymentBase> {

  modalRef: BsModalRef;
  constructor(
    protected subscriptionService: PaymentService,
    private interaction: InteractionService,
    private modalService: BsModalService
  ) {
    super();
    this.load();
  }

  public transactions: PaymentBase[] = []
  public loadingPaymentTransactions = false;

  public async load() {
    const { data } = await this.subscriptionService.search(
      this._page
    );
    this._list = data;
  }

  public async loadPaymentHistory(template: any, id: number) {
    this.transactions = [];
    this.loadingPaymentTransactions = true;
    const { data, error } = await this.subscriptionService.getPaymentTransactions(id);
    this.loadingPaymentTransactions = false;
    if (!error) {
      this.transactions = data;
    }
    this.modalRef = this.modalService.show(template,
      Object.assign({}, { class: 'gray modal-xl' }));
  }

}
