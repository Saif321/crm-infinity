import { Injectable } from '@angular/core';

import { FitApiClientService, FitList } from '@shared/fit-api-client.service';

interface PaymentBase {
  method: string;
  status: string;
  transaction_id: string;
  details: any;
  charged_value: string;
  currency_code: string;
  package_name: string;
  updated_at: string;
  created_at: string;
  number_of_history_items: number;
}

export { FitList, PaymentBase }

@Injectable()
export class PaymentService {
  constructor(
    private fitApi: FitApiClientService) { }

  public search(page: number) {
    var params: any = {
      page: page.toString()
    }
    return this.fitApi.get<FitList<PaymentBase>>('/admin/reports/payments', params);
  }

  public getPaymentTransactions(transactionId: number) {
    return this.fitApi.get<PaymentBase[]>('/admin/reports/payment-history/' + transactionId);
  }

}
