import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';


import { PopoverModule } from 'ngx-bootstrap/popover';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgSelectModule } from '@ng-select/ng-select';

import { SharedModule } from '@shared/shared.module';
import { InteractionModule } from '@app/interaction/interaction.module';

import { PaymentRoutingModule } from './payment-routing.module';
import { ListComponent } from './list/list.component';
import { PaymentService } from './payment.service';


@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    PaymentRoutingModule,
    ModalModule.forRoot(),
    FormsModule,
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    NgSelectModule,
    SharedModule,
    InteractionModule
  ],
  providers: [PaymentService]
})
export class PaymentModule { }
