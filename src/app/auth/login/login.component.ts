import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthorizationService } from '@shared/authorization.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    constructor(private authorizationService: AuthorizationService, private router: Router, private activeRoute: ActivatedRoute) {
        if (this.authorizationService.isAuthenticated()) {
            router.navigateByUrl('/');
        }
    }

    public username: string;

    public password: string;

    public async login() {
        const { error } = await this.authorizationService.login(this.username, this.password);
        if (!error) {
            this.router.navigateByUrl('/');
        }
    }

    public async refreshToken() {
        await this.authorizationService.refreshToken();
    }
}
