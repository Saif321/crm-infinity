function contains(password: string, allowedChars: string) {
    for (var i = 0; i < password.length; i++) {
        let char = password.charAt(i);
        if (allowedChars.indexOf(char) >= 0) { return true; }
    }
    return false;
}

export function isStrongPassword(password) {
    let uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let lowercase = "abcdefghijklmnopqrstuvwxyz";
    let splChars = "!@#$%&*()";
    let digits = "0123456789";

    let upperCaseFlag = contains(password, uppercase);
    let lowerCaseFlag = contains(password, lowercase);
    let digitsFlag = contains(password, digits);
    let splCharsFlag = contains(password, splChars);

    return password.length >= 8 && upperCaseFlag && lowerCaseFlag && digitsFlag && splCharsFlag
}