import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthorizationService } from '@shared/authorization.service';
import { NotificationsService } from '@shared/notifications.service';
import { isStrongPassword } from '../password-complexity';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent {
    constructor(private auth: AuthorizationService, private router: Router, private notificationsService: NotificationsService) { }

    public email = '';
    public verificationCode = '';
    public newPassword = '';
    public confirmPassword = '';

    public async resetPassword() {
        if (this.newPassword !== this.confirmPassword) {
            this.notificationsService.warning('Confirm password', 'Confirm password and password does not match');
            return;
        }

        if (!isStrongPassword(this.newPassword)) {
            this.notificationsService.warning('Password complexity', 'Password not complex enough');
            return;
        }

        const { error } = await this.auth.changePassword({
            code: this.verificationCode,
            destination: this.email,
            password: this.newPassword
        });

        if (!error) {
            this.notificationsService.success('Success', 'Password successfully changed!');
            this.router.navigateByUrl('/auth/login');
        }
    }

}
