import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthorizationService } from '@shared/authorization.service';
import { NotificationsService } from '@shared/notifications.service';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {
    constructor(private auth: AuthorizationService, private router: Router, private notificationsService: NotificationsService) { }

    public email = '';

    public async requestOneTimePassword() {
        const { error } = await this.auth.requestOneTimePassword(this.email);
        if (!error) {
            this.notificationsService.warning('Warning', 'Verification code is sent to your email.');
            this.router.navigateByUrl('/auth/reset-password');
        }
    }

}
