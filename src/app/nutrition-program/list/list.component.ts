import { Component, ViewChildren, QueryList } from '@angular/core';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';
import { UserService } from '@app/user/user.service';
import { CloudMessagingService } from '@shared/shared.service';

import { NutritionProgramService, UserListItem } from '../nutrition-program.service';
import { NutritionProgramBaseResponse } from '../types';
import { AssignNutritionProgramComponent } from '../assign-nutrition-program/assign-nutrition-program.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})

export class ListComponent extends PageableComponent<any> {
    constructor(
        protected nutritionProgramService: NutritionProgramService,
        protected userService: UserService,
        protected cloudMessagingService: CloudMessagingService,
        private interaction: InteractionService
    ) {
        super();
        this.load();
    }

    @ViewChildren(AssignNutritionProgramComponent)
    public assignProgramButtons: QueryList<AssignNutritionProgramComponent>;

    public nutritionProgramUsers: UserListItem[] = []
    public loadingNutritionProgramUsers = false;

    public async publish(nutritionProgram: NutritionProgramBaseResponse) {
        let isConfirmed = await this.interaction.confirm(nutritionProgram.is_published ? 'Are you sure you want to unpublish exercise?' : 'Are you sure you want to publish exercise?');
        if (isConfirmed) {
            await this.nutritionProgramService.publish(nutritionProgram.id);
            this.load();
        }
    }

    public onAssignPopoverOpen(cmp: AssignNutritionProgramComponent) {
        this.assignProgramButtons.forEach(button => {
            if (button !== cmp) {
                button.close();
            }
        })
    }

    public async load() {
        const { data } = await this.nutritionProgramService.search(
            this.searchQuery,
            this._page
        );
        this._list = data;
    }

    public async delete(id: number) {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to delete nutrition program?'
        )
        if (isConfirmed) {
            await this.nutritionProgramService.delete(id);
            this.load();
        }
    }

    public async loadNutritionProgramUsers(nutritionProgram: NutritionProgramBaseResponse) {
        this.nutritionProgramUsers = [];
        this.loadingNutritionProgramUsers = true;
        const { data, error } = await this.nutritionProgramService.getNutritionProgramUsers(nutritionProgram.id);
        this.loadingNutritionProgramUsers = false;
        if (!error) {
            this.nutritionProgramUsers = data;
        }
    }
    public async notify(id: number) {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to push this notification?'
        );
        if (isConfirmed) {
            await this.cloudMessagingService.notify({ object_id: id, object_type: 'NutritionProgram' });
        }
    }
}
