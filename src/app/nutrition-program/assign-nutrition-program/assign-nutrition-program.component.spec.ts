import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignNutritionProgramComponent } from './assign-nutrition-program.component';

describe('AssignNutritionProgramComponent', () => {
  let component: AssignNutritionProgramComponent;
  let fixture: ComponentFixture<AssignNutritionProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignNutritionProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignNutritionProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
