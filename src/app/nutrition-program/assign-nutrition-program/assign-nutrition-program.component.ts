import { Component, EventEmitter, Input, Output } from '@angular/core';
import { distinctUntilChanged, debounceTime } from 'rxjs/operators';

import { NotificationsService } from '@shared/notifications.service';

import { UserService } from '@app/user/user.service';
import { NutritionProgramService, UserListItem } from '../nutrition-program.service';
import { NutritionProgramBaseResponse, NutritionProgramTranslation } from '../types';
import { PopoverDirective } from 'ngx-bootstrap/popover';

@Component({
    selector: 'app-assign-nutrition-program',
    templateUrl: './assign-nutrition-program.component.html',
    styleUrls: ['./assign-nutrition-program.component.scss']
})
export class AssignNutritionProgramComponent {
    constructor(
        protected nutritionProgramService: NutritionProgramService,
        protected userService: UserService,
        private notification: NotificationsService

    ) {
        this.initUserSearch();
    }


    @Input() public nutritionProgram: NutritionProgramTranslation;
    @Output() public popoverOpen = new EventEmitter<AssignNutritionProgramComponent>(true);
    public pop: PopoverDirective;

    public usersTypeAhead = new EventEmitter<string>();
    public users: UserListItem[] = [];
    public selectedUser: UserListItem;

    public async assignNutritionProgramToUser(
        nutritionProgram: NutritionProgramBaseResponse,
        selectedUser: UserListItem,
        pop: PopoverDirective) {
        const { data, error } = await this.nutritionProgramService.assignNutritionProgramToUser(nutritionProgram.id, selectedUser.id);
        if (!error) {
            this.notification.success('Success', nutritionProgram.name + ' assigned successfuly to ' + selectedUser.first_name + ' ' + selectedUser.last_name);
            pop.hide();
        }
    }

    public close() {
        this.pop.hide();
    }

    public onOpen() {
        this.popoverOpen.emit(this);
    }

    private initUserSearch() {
        this.usersTypeAhead.pipe(
            distinctUntilChanged(),
            debounceTime(400)
        ).subscribe(term => {
            this.userService.searchUser(term, 1)
                .then(({ data, error }) => {
                    this.users = [];
                    if (!error) {
                        this.users = data.data;
                    }
                });
        });
    }
}
