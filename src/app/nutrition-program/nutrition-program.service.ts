import { Injectable } from '@angular/core';
import { FitApiClientService, FitList } from '@shared/fit-api-client.service';
import { MealGroupService, MealGroup } from '@app/meal-group/meal-group.service';
import { GoalService, GoalBase } from '@app/goal/goal.service';
import { UserListItem } from '@app/user/user.service';

import { NutritionProgramBaseResponse, NutritionProgramRequest, NutritionProgramDetailsResponse } from './types';

export { MealGroup, GoalBase, UserListItem };

@Injectable()
export class NutritionProgramService {
    constructor(private fitApi: FitApiClientService, private mealGroupService: MealGroupService, private goalService: GoalService) { }

    public search(query: string, page: number) {
        return this.fitApi.get<FitList<NutritionProgramBaseResponse>>('/admin/nutrition-program', {
            search: query,
            page: page.toString()
        });
    }

    public get(id: number) {
        return this.fitApi.get<NutritionProgramDetailsResponse>('/admin/nutrition-program/' + id);
    }

    public getNutritionProgramUsers(nutritionProgramId: number) {
        return this.fitApi.get<UserListItem[]>(`/admin/nutrition-program/${nutritionProgramId}/users`);
    }

    public create(nutritionProgram: NutritionProgramRequest) {
        return this.fitApi.put<NutritionProgramBaseResponse>('/admin/nutrition-program/0', nutritionProgram);
    }

    public assignNutritionProgramToUser(nutritionProgramId: number, userId: number) {
        return this.fitApi.put<any>(`/admin/nutrition-program/${nutritionProgramId}/user/${userId}`, undefined);
    }

    public update(nutritionProgram: NutritionProgramRequest) {
        return this, this.fitApi.put<NutritionProgramBaseResponse>('/admin/nutrition-program/' + nutritionProgram.id, nutritionProgram);
    }

    public delete(id: number) {
        return this.fitApi.delete<any>('/admin/nutrition-program/' + id);
    }

    public publish(id: number) {
        return this.fitApi.put<any>(`/admin/nutrition-program/${id}/publish`, null);
    }

    public searchMealGroups(query: string, page: number) {
        return this.mealGroupService.search(query, page);
    }

    public searchGoals(query: string, page: number) {
        return this.goalService.search(query, page);
    }
}
