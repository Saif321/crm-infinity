import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgSelectModule } from '@ng-select/ng-select';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { PopoverModule } from 'ngx-bootstrap/popover';

import { SharedModule } from '@shared/shared.module';
import { InteractionModule } from '@app/interaction/interaction.module';
import { GalleryModule } from '@app/gallery/gallery.module';
import { MealModule } from '@app/meal/meal.module';

import { NutritionProgramRoutingModule } from './nutrition-program-routing.module';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { NutritionProgramService } from './nutrition-program.service';
import { MealGroupComponent } from './form/meal-group/meal-group.component';
import { UserModule } from '@app/user/user.module';
import { AssignNutritionProgramComponent } from './assign-nutrition-program/assign-nutrition-program.component';

@NgModule({
    declarations: [FormComponent, ListComponent, MealGroupComponent, AssignNutritionProgramComponent],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        PaginationModule.forRoot(),
        InteractionModule,
        NutritionProgramRoutingModule,
        NgSelectModule,
        GalleryModule,
        DragDropModule,
        UserModule,
        PopoverModule.forRoot(),
        MealModule
    ],
    providers: [NutritionProgramService]
})
export class NutritionProgramModule { }
