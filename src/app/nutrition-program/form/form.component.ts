import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { distinctUntilChanged, debounceTime } from 'rxjs/operators';

import { MealGroupBase } from '@app/meal-group/meal-group.service';
import { GoalBase } from '@app/goal/goal.service';
import { NotificationsService } from '@shared/notifications.service';
import { LanguagesComponent } from '@shared/components/languages/languages.component';

import { NutritionProgram, NutritionProgramTranslation, NutritionProgramStatus, statuses, NutritionProgramGroup, NutritionProgramRequest, NutritionProgramRequestTranslation, NutritionProgramGroupItemRequest, NutritionProgramItemRequest } from '../types';
import { NutritionProgramService } from '../nutrition-program.service';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
    constructor(
        private nutritionProgramService: NutritionProgramService,
        private route: ActivatedRoute,
        private router: Router,
        private notifications: NotificationsService
    ) {
        this.initMealGroupSearch();
        this.initEnvironmentsSearch();
    }

    public selectedLanguageId = 1;
    public isDisabled = false;

    public mealGroupsTypeAhead = new EventEmitter<string>();
    public mealGroups: MealGroupBase[] = [];
    public selectedMealGroup: MealGroupBase;

    public goalsTypeAhead = new EventEmitter<string>();
    public goals: GoalBase[] = [];

    public statuses: NutritionProgramStatus[] = statuses;

    @ViewChild(LanguagesComponent, { static: true })
    languagesComponent: LanguagesComponent;

    public isEditMode = false;

    public get isDefaultLanguage() {
        return this.languagesComponent.isDefaultLanguage;
    }

    public get nutritionProgram() {
        return this._nutritionProgram;
    }

    public get nutritionProgramTranslation(): NutritionProgramTranslation {
        const nutritionProgram = this._nutritionProgram;
        if (!nutritionProgram) {
            return;
        }
        let translation = nutritionProgram.translations.find(
            t => t.language_id === this.selectedLanguageId
        );
        if (!translation) {
            translation = {
                name: '',
                description: '',
                language_id: this.selectedLanguageId,
                image: null,
                video: null
            };
            nutritionProgram.translations.push(translation);
        }
        return translation;
    }

    public onAddMealGroupClick() {
        if (!this.selectedMealGroup) {
            return;
        }
        if (this._nutritionProgram.groups.some(g => g.meal_group.id == this.selectedMealGroup.id)) {
            this.notifications.warning('Duplicate meal group', 'Same meal group can not be added twice in program');
            return;
        }
        this._nutritionProgram.groups.push({
            id: undefined,
            meal_group: this.selectedMealGroup,
            meals: []
        });
    }

    public onMealGroupDrop(event: CdkDragDrop<NutritionProgramGroup[]>) {
        moveItemInArray(this._nutritionProgram.groups, event.previousIndex, event.currentIndex);
    }


    public deleteMealGroup(group: NutritionProgramGroup) {
        this._nutritionProgram.groups.splice(this._nutritionProgram.groups.indexOf(group), 1);
    }

    public onNutritionProgramSubmit() {
        if (this.isEditMode) {
            this.update();
        } else {
            this.create();
        }
    }

    public async ngOnInit() {
        this.route.paramMap.subscribe(params => {
            if (!params.has('id')) {
                this.isEditMode = false;
                return;
            }
            this.isEditMode = true;
            this.nutritionProgramService.get(parseInt(params.get('id'))).then(res => {
                const p = res.data;
                this.goals = p.goals;
                this._nutritionProgram = {
                    id: p.id,
                    is_free: p.is_free,
                    is_published: p.is_published,
                    program_length: p.program_length,
                    goal_ids: p.goals.map(g => g.id),
                    groups: p.groups.map((g): NutritionProgramGroup => {
                        return {
                            id: g.id,
                            meal_group: g.meal_group,
                            meals: g.items.sort((a, b) => {
                                return a.order > b.order ? 1 : -1;
                            }).map(i => i.meal)
                        }
                    }),
                    translations: p.translations
                };
            });
        });
    }

    public async create() {
        this.isDisabled = true;
        const { data, error } = await this.nutritionProgramService.create(this.getRequest());
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'Create was successful!');
            this.router.navigate(['/nutrition-program/edit', data.id]);
        }
    }

    public async update() {
        this.isDisabled = true;
        const { data, error } = await this.nutritionProgramService.update(this.getRequest());
        this.isDisabled = false;
        if (!error) {
            this.notifications.success('Success', 'The update was successful!');
        }
    }

    private initMealGroupSearch() {
        this.mealGroupsTypeAhead.pipe(
            distinctUntilChanged(),
            debounceTime(400)
        ).subscribe(term => {
            this.nutritionProgramService.searchMealGroups(term, 1)
                .then(({ data, error }) => {
                    this.mealGroups = [];
                    if (!error) {
                        this.mealGroups = data.data;
                    }
                });
        });
    }

    private initEnvironmentsSearch() {
        this.goalsTypeAhead.pipe(
            distinctUntilChanged(),
            debounceTime(400)
        ).subscribe(term => {
            this.nutritionProgramService.searchGoals(term, 1)
                .then(({ data, error }) => {
                    this.mealGroups = [];
                    if (!error) {
                        this.goals = data.data;
                    }
                });
        });
    }

    private getDefaultTranslation() {
        return this._nutritionProgram.translations.find(t => t.language_id === this.languagesComponent.defaultLanguage.id);
    }

    private getRequest(): NutritionProgramRequest {
        const n = this._nutritionProgram;
        const t = this.getDefaultTranslation();
        return {
            id: n.id,
            name: t.name,
            description: t.description,
            image_id: t.image.id,
            video_id: t.video ? t.video.id : null,
            is_free: n.is_free,
            is_published: n.is_published,
            program_length: n.program_length,
            goal_ids: n.goal_ids,
            groups: n.groups.map((g, i): NutritionProgramGroupItemRequest => {
                return {
                    meal_group_id: g.meal_group.id,
                    order: i + 1,
                    items: g.meals.map((m, i): NutritionProgramItemRequest => {
                        return {
                            meal_id: m.id,
                            order: i + 1
                        };
                    })
                }
            }),
            translations: n.translations.filter(t => t.name && t.description)
                .map((t): NutritionProgramRequestTranslation => {
                    return {
                        language_id: t.language_id,
                        name: t.name,
                        description: t.description,
                        image_id: t.image.id,
                        video_id: t.video ? t.video.id : null
                    }
                })
        }
    }

    private _nutritionProgram: NutritionProgram = {
        goal_ids: [],
        groups: [],
        is_published: false,
        is_free: false,
        program_length: 0,
        translations: []
    };
}
