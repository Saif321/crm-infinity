import { Component, Input, EventEmitter, Output } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { MealService, MealBase } from '@app/meal/meal.service';
import { NotificationsService } from '@shared/notifications.service';
import { NutritionProgramGroup } from './../../types';

@Component({
    selector: 'meal-group',
    templateUrl: './meal-group.component.html',
    styleUrls: ['./meal-group.component.scss']
})
export class MealGroupComponent {

    constructor(
        private mealService: MealService,
        private notifications: NotificationsService) { }

    @Input()
    public get group() { return this._group; }
    public set group(group) {
        this._group = group;
        if (!group.id) {
            this.isCollapsed = false;
        }
    }

    @Input() public isDefaultLanguage = false;

    @Output() public deleteMealGroup = new EventEmitter<NutritionProgramGroup>(true);

    public isCollapsed = true;

    public onDeleteGroupClick() {
        this.deleteMealGroup.emit(this.group);
    }

    public async addMeal(group: NutritionProgramGroup) {
        const meal = await this.mealService.selectFromMeals();
        if (meal) {
            if (group.meals.some(m => meal.id == m.id)) {
                this.notifications.warning('Duplicate meal', 'Meal can not be added twice in group');
                return;
            }
            group.meals.push(meal);
        }
    }

    public deleteMeal(group: NutritionProgramGroup, meal: MealBase) {
        group.meals.splice(group.meals.indexOf(meal), 1);
    }

    public onMealItemDrop(event: CdkDragDrop<MealBase[]>) {
        moveItemInArray(this.group.meals, event.previousIndex, event.currentIndex);
    }

    private _group: NutritionProgramGroup;
}
