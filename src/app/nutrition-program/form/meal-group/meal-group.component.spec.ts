import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealGroupComponent } from './meal-group.component';

describe('MealGroupComponent', () => {
  let component: MealGroupComponent;
  let fixture: ComponentFixture<MealGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MealGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
