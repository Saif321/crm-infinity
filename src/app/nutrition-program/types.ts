import { GalleryItem } from '@app/gallery/gallery.service';
import { MealBase } from '@app/meal/meal.service';
import { MealGroupBase } from '@app/meal-group/meal-group.service';
import { GoalBase } from '@app/goal/goal.service';

export interface NutritionProgramBaseResponse {
    id: number;
    name: string;
    description: string;
    video_id: number;
    image_id: number;
    program_length: number;
    meals_per_day: number;
    comment_count: number;
    like_count: number;
    review_count: number;
    avg_rating: number;
    is_published: boolean;
    is_free: boolean;
    created_at: string;
    updated_at: string;
    activated_count: number;
    deactivated_count: number;
}

export interface NutritionProgramDetailsTranslation extends NutritionProgramBaseResponse {
    language_id: number;
    image: GalleryItem;
    video: GalleryItem;
}

interface NutritionProgramDetailsGroupItemResponse {
    created_at: string
    id: number;
    meal: MealBase;
    meal_id: number;
    nutrition_program_meal_group_id: number;
    order: number;
    updated_at: string;
}

interface NutritionProgramDetailsGroupResponse {
    id: number;
    meal_group_id: number;
    nutrition_program_id: number;
    order: number;
    updated_at: string;
    meal_group: MealGroupBase;
    items: NutritionProgramDetailsGroupItemResponse[];
}

export interface NutritionProgramDetailsResponse extends NutritionProgramBaseResponse {
    image: GalleryItem;
    video: GalleryItem;
    translations: NutritionProgramDetailsTranslation[];
    goals: GoalBase[];
    groups: NutritionProgramDetailsGroupResponse[];
}

export interface NutritionProgramItemRequest {
    meal_id: number;
    order: number;
}

export interface NutritionProgramGroupItemRequest {
    meal_group_id: number;
    order: number;
    items: NutritionProgramItemRequest[]
}

export interface NutritionProgramRequestTranslation {
    language_id?: number;
    name: string;
    description: string;
    image_id: number;
    video_id: number;
}

export interface NutritionProgramRequest {
    id?: number;
    name: string;
    description: string;
    is_published: boolean;
    is_free: boolean;
    program_length: number;
    goal_ids: number[];
    image_id: number;
    video_id: number;
    translations: NutritionProgramRequestTranslation[];
    groups: NutritionProgramGroupItemRequest[];
}

export interface NutritionProgramGroup {
    id?: number;
    meal_group: MealGroupBase;
    meals: MealBase[];
}

export interface NutritionProgramTranslation {
    language_id?: number;
    name: string;
    description: string;
    image: GalleryItem;
    video: GalleryItem;
}

export interface NutritionProgram {
    id?: number;
    is_published: boolean;
    program_length: number;
    goal_ids: number[];
    is_free: boolean;
    translations: NutritionProgramTranslation[];
    groups: NutritionProgramGroup[];
}

interface NutritionProgramStatus {
    name: string;
    value: boolean;
}

const statuses: NutritionProgramStatus[] = [
    { value: false, name: 'Draft' },
    { value: true, name: 'Published' }
];

export { NutritionProgramStatus, statuses };