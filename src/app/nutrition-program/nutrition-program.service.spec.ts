import { TestBed } from '@angular/core/testing';

import { NutritionProgramService } from './nutrition-program.service';

describe('NutritionProgramService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: NutritionProgramService = TestBed.inject(NutritionProgramService);
        expect(service).toBeTruthy();
    });
});
