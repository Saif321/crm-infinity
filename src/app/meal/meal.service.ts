import { Injectable } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { FitApiClientService, FitList } from '@shared/fit-api-client.service';
import { GalleryItem } from '@app/gallery/gallery.service';

import { ModalComponent } from './modal/modal/modal.component';

export interface RecipeStepTranslation {
    language_id: number;
    description: string;
}

export interface RecipeStep {
    description: string;
    order: number;
    translations: RecipeStepTranslation[];
}

export interface IngredientTranslation {
    language_id: number;
    name: string;
}

export interface Ingredient {
    name: string;
    translations: IngredientTranslation[];
}

interface MealBase {
    id?: number;
    name: string;
    description: string;
    carbohydrates: number;
    fat: number;
    image_id: number;
    is_published: boolean;
    number_of_calories: number;
    preparation_time_in_minutes: number;
    proteins: number;
    video_id: number;
    created_at?: string;
    updated_at?: string;
    ingredients: Ingredient[];
    steps: RecipeStep[];
}

interface MealTranslation {
    language_id: number;
    name: string;
    description: string;
    image_id: number;
    video_id: number;
    image?: GalleryItem;
    video?: GalleryItem;
    object_id?: number;
}

interface Meal extends MealBase {
    translations: MealTranslation[];
}

interface MealResponse extends Meal {
    recipes: RecipeStep[];
}

export { FitList, MealBase, Meal, MealTranslation };

@Injectable()
export class MealService {
    constructor(private fitApi: FitApiClientService,
        private modalService: BsModalService) { }

    private bsModalRef: BsModalRef;
    public search(query: string, page: number) {
        return this.fitApi.get<FitList<MealBase>>('/admin/meal', {
            search: query,
            page: page.toString()
        });
    }
    public get(id: number) {
        return this.fitApi.get<MealResponse>('/admin/meal/' + id);
    }
    public create(meal: Meal) {
        return this.fitApi.put<MealResponse>('/admin/meal/0', this.mapToRequest(meal));
    }
    public update(meal: Meal) {
        return this, this.fitApi.put<MealResponse>('/admin/meal/' + meal.id, this.mapToRequest(meal));
    }
    public delete(id: number) {
        return this.fitApi.delete<any>('/admin/meal/' + id);
    }

    public selectFromMeals(): Promise<Meal> {
        const promise = new Promise<Meal>((resolve, reject) => {
            const initialState = {
                onClose: (item: Meal) => {
                    resolve(item);
                },
            };
            this.bsModalRef = this.modalService.show(ModalComponent, { initialState, backdrop: false, ignoreBackdropClick: true, class: 'meal-modal' });
        })
        return promise;
    }

    private mapToRequest(meal: Meal): Meal {
        return {
            name: meal.name,
            description: meal.description,
            is_published: meal.is_published,
            carbohydrates: meal.carbohydrates,
            fat: meal.fat,
            image_id: meal.image_id,
            number_of_calories: meal.number_of_calories,
            video_id: meal.video_id,
            preparation_time_in_minutes: meal.preparation_time_in_minutes,
            proteins: meal.proteins,
            ingredients: meal.ingredients.map(i => {
                return {
                    name: i.name,
                    translations: i.translations.filter(t => !!t.name)
                        .map(t => {
                            return {
                                language_id: t.language_id,
                                name: t.name
                            }
                        })
                }
            }),
            steps: meal.steps.map(s => {
                return {
                    description: s.description,
                    order: s.order,
                    translations: s.translations.filter(t => !!t.description)
                        .map(t => {
                            return {
                                language_id: t.language_id,
                                description: t.description
                            };
                        })
                }
            }),
            translations: meal.translations.filter(t => t.name && t.description && t.image_id)
                .map(t => {
                    return {
                        language_id: t.language_id,
                        name: t.name,
                        description: t.description,
                        image_id: t.image_id,
                        video_id: t.video_id
                    }
                })
        }
    }
}
