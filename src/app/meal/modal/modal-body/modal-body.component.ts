import { Component, Output, EventEmitter, Input } from '@angular/core';

import { PageableComponent } from '@shared/pageable.component';

import { MealService, MealBase } from '@app/meal/meal.service';

@Component({
  selector: 'modal-body',
  templateUrl: './modal-body.component.html',
  styleUrls: ['./modal-body.component.scss']
})
export class ModalBodyComponent extends PageableComponent<MealBase> {

  constructor(
    protected mealService: MealService
  ) {
    super();
    this.load();
  }

  @Output() public itemSelected = new EventEmitter<MealBase>(true);

  @Input() public selected: MealBase;

  public async load() {
    const { data } = await this.mealService.search(
      this.searchQuery,
      this._page,
    );
    this._list = data;
  }

  public select(item: MealBase) {
    this.selected = item;
    this.itemSelected.emit(item);
  }

}
