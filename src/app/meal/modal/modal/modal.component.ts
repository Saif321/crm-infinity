import { Component } from '@angular/core';


import { Meal } from '@app/meal/meal.service';
import { BsModalRef } from 'ngx-bootstrap/modal';


@Component({
    selector: 'meal-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
    constructor(
        public bsModalRef: BsModalRef
    ) { }

    public touched = false;
    public isDisabled = false;

    public select(item: Meal) {
        this.onClose(item);
        this.bsModalRef.hide();
    }

    private onClose(item: Meal) { }
}
