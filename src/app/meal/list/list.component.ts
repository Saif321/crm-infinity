import { Component, OnInit } from '@angular/core';

import { PageableComponent } from '@shared/pageable.component';
import { InteractionService } from '@app/interaction/interaction.service';

import { MealService, MealBase } from '../meal.service';

@Component({
    selector: 'meal-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent extends PageableComponent<MealBase> {

    constructor(
        protected mealService: MealService,
        private interaction: InteractionService
    ) {
        super();
        this.load();
    }

    public isPublished: boolean;

    public async load() {
        const { data } = await this.mealService.search(
            this.searchQuery,
            this._page
        );
        this._list = data;
    }

    public async delete(id: number) {
        const isConfirmed = await this.interaction.confirm(
            'Are you sure you want to delete meal?'
        )
        if (isConfirmed) {
            await this.mealService.delete(id);
            this.load();
        }
    }
}
