import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { GalleryModule } from '@app/gallery/gallery.module';
import { SharedModule } from '@shared/shared.module';

import { PaginationModule } from 'ngx-bootstrap/pagination';

import { MealRoutingModule } from './meal-routing.module';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { MealService } from './meal.service';
import { ModalComponent } from './modal/modal/modal.component';
import { ModalBodyComponent } from './modal/modal-body/modal-body.component';


@NgModule({
    declarations: [FormComponent, ListComponent, ModalComponent, ModalBodyComponent],
    imports: [
        CommonModule,
        FormsModule,
        MealRoutingModule,
        SharedModule,
        GalleryModule,
        PaginationModule.forRoot(),
        NgSelectModule
    ],
    providers: [MealService]
})
export class MealModule { }
