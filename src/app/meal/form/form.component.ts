import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { LanguagesComponent } from '@shared/components/languages/languages.component';

import { MealService, Meal, Ingredient, IngredientTranslation, RecipeStep, RecipeStepTranslation } from '../meal.service';
import { NotificationsService } from '@shared/notifications.service';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
    constructor(
        private mealService: MealService,
        private route: ActivatedRoute,
        private router: Router,
        private notificationService: NotificationsService) { }

    public selectedLanguageId = 1;

    @ViewChild(LanguagesComponent, { static: true })
    public languagesComponent: LanguagesComponent;

    public isEditMode = false;
    public isDisabled = false;

    public get isDefaultLanguage() {
        return this.languagesComponent.isDefaultLanguage;
    }

    public get meal() { return this._meal; }

    public get mealTranslation() {
        const meal = this._meal;
        if (!meal) {
            return;
        }
        let translation = meal.translations.find(
            t => t.language_id === this.selectedLanguageId
        );
        if (!translation) {
            translation = {
                name: meal.name,
                description: meal.description,
                video_id: undefined,
                image_id: undefined,
                video: undefined,
                image: undefined,
                language_id: this.selectedLanguageId
            };
            meal.translations.push(translation);
        }
        return translation;
    }

    public get ingredients() {
        const meal = this._meal;
        if (!meal) {
            return;
        }
        return meal.ingredients.map(i => this.ingredientTranslation(i));
    }

    public ingredientTranslation(ingredient: Ingredient) {
        let translation = ingredient.translations.find(
            t => t.language_id === this.selectedLanguageId
        );
        if (!translation) {
            translation = {
                name: ingredient.name,
                language_id: this.selectedLanguageId
            };
            ingredient.translations.push(translation);
        }
        return translation;
    }

    public addIngredient() {
        this.meal.ingredients.push({
            name: '',
            translations: []
        });
    }

    public removeIngredient(ingredientTranslation: IngredientTranslation) {
        this.meal.ingredients.splice(this.meal.ingredients.findIndex(i => i.translations.some(t => t === ingredientTranslation)), 1);
    }

    public get recipeSteps() {
        const meal = this._meal;
        if (!meal) {
            return;
        }
        return meal.steps.map(i => this.recipeStepTranslation(i));
    }

    public recipeStepTranslation(recipeStep: RecipeStep) {
        let translation = recipeStep.translations.find(
            t => t.language_id === this.selectedLanguageId
        );
        if (!translation) {
            translation = {
                description: recipeStep.description,
                language_id: this.selectedLanguageId
            };
            recipeStep.translations.push(translation);
        }
        return translation;
    }

    public addRecipeStep() {
        this.meal.steps.push({
            description: '',
            order: 0,
            translations: []
        });
    }

    public removeRecipeStep(recipeStepTranslation: RecipeStepTranslation) {
        this.meal.steps.splice(this.meal.steps.findIndex(m => m.translations.some(t => t === recipeStepTranslation)), 1);
    }

    public ngOnInit() {
        this.route.paramMap.subscribe(params => {
            if (!params.has('id')) {
                this.isEditMode = false;
                return;
            }
            this.isEditMode = true;
            this.mealService.get(parseInt(params.get('id')))
                .then(res => {
                    this._meal = res.data;
                    this._meal.steps = res.data.recipes;
                    res.data.recipes = undefined;
                });
        });
    }

    public async onMealSubmit() {
        const meal = this._meal;
        const mealDefaultTranslation = meal.translations.find(t => t.language_id == this.languagesComponent.defaultLanguage.id);

        meal.translations = meal.translations.filter(t => !!t.name);
        meal.translations.forEach(t => {
            t.image_id = t.image.id;
            t.video_id = t.video ? t.video.id : null;
        })

        meal.name = mealDefaultTranslation.name;
        meal.description = mealDefaultTranslation.description;
        meal.image_id = mealDefaultTranslation.image_id;
        meal.video_id = mealDefaultTranslation.video_id;

        meal.ingredients.forEach(ingredient => {
            const ingredientDefaultTranslation = ingredient.translations.find(i => i.language_id == this.languagesComponent.defaultLanguage.id);
            ingredient.name = ingredientDefaultTranslation.name;
        });

        meal.steps.forEach((ingredient, index) => {
            const ingredientDefaultTranslation = ingredient.translations.find(i => i.language_id == this.languagesComponent.defaultLanguage.id);
            ingredient.description = ingredientDefaultTranslation.description;
            ingredient.order = index;
        });
        if (!this.isEditMode) {
            this.isDisabled = true;
            const { data, error } = await this.mealService.create(meal);
            this.isDisabled = false;

            if (!error) {
                this.notificationService.success('Success', 'Create was successful!');
                this.router.navigate(['/meal/edit', data.id]);
            }
        } else {
            this.isDisabled = true;
            const { data, error } = await this.mealService.update(meal);
            this.isDisabled = false;

            if (!error) {
                this.notificationService.success('Success', 'The update was successful!');
            }
        }


    }

    private _meal: Meal = {
        name: '',
        description: '',
        carbohydrates: 0,
        fat: 0,
        image_id: 0,
        number_of_calories: 0,
        is_published: false,
        preparation_time_in_minutes: 0,
        proteins: 0,
        video_id: 0,
        created_at: '',
        updated_at: '',
        translations: [],
        ingredients: [],
        steps: []
    }
}
