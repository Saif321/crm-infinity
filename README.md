# FitCms

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.6.

## Install dependencies

Run  `npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` or `npm run build` or `npm run build:production` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
Check package.json for further build tasks.

Build output can be found in `dist/fit-cms`

## Configuration

Application configuration files are located inside `src/assets/config` and used by `ng build` through `src/environments`

## JWT module configuration

JWT module can be configured in `src/app/app.module.ts` and most important path is 

```js
JwtModule.forRoot({
    config: {
        tokenGetter,
        whitelistedDomains: ['localhost', 'fit-api.telego.rs', 'api.infinityfit.io'],
        blacklistedRoutes: []
    }
})
```

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
